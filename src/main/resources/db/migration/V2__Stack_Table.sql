CREATE TABLE stack (
    id IDENTITY NOT NULL PRIMARY KEY,
    stack_name VARCHAR NOT NULL,
    stack_id INT NOT NULL,
    swarm_id INT,
    image_id INT,
    foreign key(swarm_id) references swarm(id),
    foreign key(image_id) references IMAGE(id)
)