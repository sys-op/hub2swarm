CREATE TABLE cred (
    id IDENTITY NOT NULL PRIMARY KEY,
    username VARCHAR NOT NULL,
    pass VARCHAR NOT NULL,
    swarm_id INT,
    foreign key(swarm_id) references swarm(id)
)