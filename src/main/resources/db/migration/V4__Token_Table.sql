CREATE TABLE token (
    id IDENTITY NOT NULL PRIMARY KEY,
    token VARCHAR NOT NULL,
    expiration_time BIGINT NOT NULL,
    cred_id INT,
    foreign key(cred_id) references cred(id)
)