CREATE TABLE deployment
(
    id            IDENTITY NOT NULL PRIMARY KEY,
    deployment_id VARCHAR  NOT NULL UNIQUE,
    image         VARCHAR,
    status        TINYINT  NOT NULL,
    message       VARCHAR  NOT NULL,
    created       BIGINT   NOT NULL
);