package eu.yatech.hub2swarm.model;

import lombok.*;

import javax.persistence.*;

@Entity
@RequiredArgsConstructor(staticName = "newSwarm")
@AllArgsConstructor
@NoArgsConstructor
public @Data
class Swarm {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    @NonNull private String label;

    @Column(unique = true, nullable = false)
    @NonNull private String url;
}
