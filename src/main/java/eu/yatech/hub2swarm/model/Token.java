package eu.yatech.hub2swarm.model;

import lombok.*;

import javax.persistence.*;

@Entity
@RequiredArgsConstructor(staticName = "newToken")
@AllArgsConstructor
@NoArgsConstructor
public @Data
class Token {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false)
    @NonNull
    private String token;

    @Column(nullable = false)
    @NonNull
    private long expirationTime;

    @ManyToOne
    @JoinColumn(name = "cred_id", nullable = false)
    @NonNull
    private Cred cred;
}
