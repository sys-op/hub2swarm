package eu.yatech.hub2swarm.model;

import com.vaadin.flow.component.icon.Icon;
import eu.yatech.hub2swarm.app.DeploymentStatus;
import eu.yatech.hub2swarm.utils.Utils;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;

@Entity
@NoArgsConstructor
public @Data
class Deployment {

    public static Deployment newDeployment(String deploymentId, String image, byte status, String message) {
        Deployment d = new Deployment();
        d.deploymentId = deploymentId;
        d.image = image;
        d.status = status;
        d.message = message;
        d.created = System.currentTimeMillis();
        return d;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @NonNull
    private String deploymentId;

    private String image;

    @Column(nullable = false)
    @NonNull
    private byte status;

    @Column(nullable = false)
    @NonNull
    private String message;

    @Column(nullable = false)
    @NonNull
    private long created;

    public Icon getDeploymentStatusIcon() {
        return new Icon(getDeploymentStatus().getIcon());
    }

    public String time() {
        return Utils.millisToHumanReadableTime(this.created);
    }

    public DeploymentStatus getDeploymentStatus() {
        DeploymentStatus defaultStatus = DeploymentStatus.FAIL;
        switch (this.status) {
            case 0:
                return DeploymentStatus.OK;
            case 1:
                return DeploymentStatus.FAIL;
            default:
                return defaultStatus;
        }
    }
}
