package eu.yatech.hub2swarm.model;

import eu.yatech.hub2swarm.app.ImageReplacePolicy;
import lombok.*;

import javax.persistence.*;

@Entity
@RequiredArgsConstructor(staticName = "newStack")
@AllArgsConstructor
@NoArgsConstructor
public @Data
class Stack {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @NonNull private String stackName;

    @Column(nullable = false)
    @NonNull private int stackId;

    @ManyToOne
    @JoinColumn(name = "swarmId", nullable = false)
    @NonNull private Swarm swarm;

    @ManyToOne
    @JoinColumn(name = "imageId", nullable = false)
    @NonNull private DockerImage image;

    @Column(nullable = false)
    @NonNull private byte imageReplacePolicy;

    public String getSwarmLabel() {
        return this.swarm.getLabel();
    }

    public String getImageLabel() {
        return this.image.getRepo()+":"+this.image.getTag();
    }

    public ImageReplacePolicy getReplacePolicy() {
        ImageReplacePolicy defaultPolicy = ImageReplacePolicy.NO_REPLACE;
        switch (this.imageReplacePolicy) {
            case 0:
                return ImageReplacePolicy.NO_REPLACE;
            case 1:
                return ImageReplacePolicy.REPLACE;
            default:
                return defaultPolicy;
        }
    }
}
