package eu.yatech.hub2swarm.model;

import lombok.*;

import javax.persistence.*;

@Entity
@RequiredArgsConstructor(staticName = "newCred")
@AllArgsConstructor
@NoArgsConstructor
public @Data
class Cred {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    @NonNull private String username;

    @Column(name="pass", nullable = false)
    @NonNull private String password;

    @ManyToOne
    @JoinColumn(name = "swarmId", nullable = false)
    @NonNull private Swarm swarm;

    public String getSwarmLabel() {
        return this.swarm.getLabel();
    }
}
