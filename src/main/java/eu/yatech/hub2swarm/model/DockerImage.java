package eu.yatech.hub2swarm.model;


import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "image")
@RequiredArgsConstructor(staticName = "newImage")
@AllArgsConstructor
@NoArgsConstructor
public @Data
class DockerImage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull private String repo;
    @NonNull private String tag;

    public String toDockerImage() {
        return repo+":"+tag;
    }
}
