package eu.yatech.hub2swarm.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "uzer") //because user is reserved word
@RequiredArgsConstructor(staticName = "newUser")
@AllArgsConstructor
@NoArgsConstructor
public @Data
class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    @NonNull
    private String username;

    @Column(nullable = false)
    @NonNull
    private String password;

    @Column(nullable = false)
    private boolean enabled = true;

    @Column(nullable = false)
    private boolean locked = false;
}
