package eu.yatech.hub2swarm.utils;

import lombok.Data;

public @Data
class Result {
    private String message;
    private Object payload;

    public static Result newResult(String message) {
        return new Result(message);
    }

    public Result withPayload(Object payload) {
        this.payload = payload;
        return this;
    }

    private Result(String message){
        this.message = message;
    }

}
