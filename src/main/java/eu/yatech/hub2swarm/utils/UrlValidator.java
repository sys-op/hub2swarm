package eu.yatech.hub2swarm.utils;

import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.validator.AbstractValidator;

public class UrlValidator extends AbstractValidator {
    private final String[] schemes = {"http", "https"};
    private final org.apache.commons.validator.routines.UrlValidator urlValidator = new org.apache.commons.validator.routines.UrlValidator(schemes);

    /**
     * Constructs a validator with the given error message. The substring "{0}"
     * is replaced by the value that failed validation.
     *
     * @param errorMessage the message to be included in a failed result, not null
     */
    public UrlValidator(String errorMessage) {
        super(errorMessage);
    }

    public boolean isValid(Object value) {
        if (value == null) {
            return false;
        }
        if (!(value instanceof String)) {
            throw new IllegalArgumentException("UrlValidator expects String input, got a " + value.getClass());
        }
        String url = (String)value;
        if (!url.startsWith("http")){
            url = "http://" + url;
        }
        return urlValidator.isValid(url);
    }

    @Override
    public ValidationResult apply(Object value, ValueContext context) {
        return null;
    }

    @Override
    public Object apply(Object o, Object o2) {
        return null;
    }

}
