package eu.yatech.hub2swarm.utils;

import com.vaadin.flow.component.notification.Notification;

public class Notifier {

    private static final int duration = 3000;
    private static final Notification.Position position = Notification.Position.MIDDLE;

    public static Notification doNotify(String message) {
        return new Notification(message, duration, position);
    }
}
