package eu.yatech.hub2swarm.utils;


import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    private Utils() {
        //util class
    }

    public static String millisToHumanReadableTime(long unixTime) {
        Date date = new Date(unixTime);
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return df.format(date);
    }
}
