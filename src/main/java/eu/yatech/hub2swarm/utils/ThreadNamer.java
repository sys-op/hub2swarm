package eu.yatech.hub2swarm.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class ThreadNamer {
    @SuppressWarnings("WeakerAccess") //to be used in future
    public static final String DEPLOYMENT_PREFIX = "deploy-";

    private ThreadNamer() {
        //util class
    }

    public static String deployerName() {
        return DEPLOYMENT_PREFIX + RandomStringUtils.randomAlphanumeric(6);
    }
}
