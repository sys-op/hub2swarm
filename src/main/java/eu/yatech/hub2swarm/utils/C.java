package eu.yatech.hub2swarm.utils;

public class C {
    public static final String NO_VALUE_MARKER = "VOID";

    public static final String SUCCESS = "GREAT SUCCESS";
    public static final String CREATED = "201";
    public static final String UPDATED = "200";

    //automagic
    public static final String HUB_AUTOMAGIC_ENV = "HUB_IMAGE_AUTOMAGIC";
    public static final String ON = "ON";
    public static final String OFF = "OFF";

    //encryption
    public static final String SECRET_KEY_PASSWORD_ENV = "ENCRYPT_KEY_PASSWORD";
    public static final String SALT_ENV = "ENCRYPT_SALT";

    public static final String SECRET_KEY_PASSWORD_PROP = "app.encrypt.password";
    public static final String SALT_PROP = "app.encrypt.salt";

    //exit code
    public static final int APPLICATION_NOT_SECURE = 3;
    public static final int NOWHERE_TO_SAVE_ROOT_PASSWORD = 4;

    //token life
    private static final long ONE_HOUR = 3600000; //in millis
    public static final long TOKEN_LIFE = 8 * ONE_HOUR;

    //first user
    public static final String FIRST_USER_USERNAME_ENV = "FIRST_USER_USERNAME";
    public static final String FIRST_USER_USERNAME_PASSWORD = "FIRST_USER_PASSWORD";
    public static final String FIRST_USER_DEFAULT_USERNAME = "root";
    public static final String GENERATED_PASSWORD_LOCATION_PROP = "app.firstUser.password.file";
}
