package eu.yatech.hub2swarm.dao;

import eu.yatech.hub2swarm.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User, Long> {

    User findFirstByUsername(String username);

    List<User> findByEnabled(boolean enabled);

    List<User> findByLocked(boolean locked);
}
