package eu.yatech.hub2swarm.dao;

import eu.yatech.hub2swarm.model.Deployment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeploymentDao extends JpaRepository<Deployment, Long> {

    List<Deployment> findAllByOrderByCreatedDesc();

    List<Deployment> findByDeploymentId(String deploymentId);

    List<Deployment> findByStatusOrderByCreatedDesc(byte status);

    Deployment findFirstByDeploymentId(String deploymentId);
}
