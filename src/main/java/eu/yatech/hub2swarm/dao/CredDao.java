package eu.yatech.hub2swarm.dao;

import eu.yatech.hub2swarm.model.Cred;
import eu.yatech.hub2swarm.model.Swarm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CredDao extends JpaRepository<Cred, Long> {
    Cred findByUsernameAndSwarm(String username, Swarm swarm);

    List<Cred> findBySwarm(Swarm swarm);
}
