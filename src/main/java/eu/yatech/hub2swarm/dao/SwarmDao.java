package eu.yatech.hub2swarm.dao;

import eu.yatech.hub2swarm.model.Swarm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SwarmDao extends JpaRepository<Swarm, Long> {

    Swarm findFirstByLabel(String label);

    Swarm findFirstByUrl(String url);

}
