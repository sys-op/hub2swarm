package eu.yatech.hub2swarm.dao;

import eu.yatech.hub2swarm.model.DockerImage;
import eu.yatech.hub2swarm.model.Stack;
import eu.yatech.hub2swarm.model.Swarm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StackDao extends JpaRepository<Stack, Long> {

    Stack findFirstByStackNameAndSwarmAndImage(String stackName, Swarm swarm, DockerImage image);

    List<Stack> findBySwarm(Swarm swarm);

    List<Stack> findByImage(DockerImage image);
}
