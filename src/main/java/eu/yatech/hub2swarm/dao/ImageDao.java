package eu.yatech.hub2swarm.dao;

import eu.yatech.hub2swarm.model.DockerImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageDao extends JpaRepository<DockerImage, Long> {

    DockerImage findFirstByRepoAndTag(String repo, String tag);

}
