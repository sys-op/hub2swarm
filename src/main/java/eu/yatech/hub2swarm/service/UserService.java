package eu.yatech.hub2swarm.service;

import eu.yatech.hub2swarm.config.security.AppUserDetails;
import eu.yatech.hub2swarm.dao.UserDao;
import eu.yatech.hub2swarm.model.User;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private final UserDao userDao;
    private final PasswordEncoder passwordEncoder;
    private final SessionRegistry sessionRegistry;

    public UserService(UserDao userDao, PasswordEncoder passwordEncoder, SessionRegistry sessionRegistry) {
        this.userDao = userDao;
        this.passwordEncoder = passwordEncoder;
        this.sessionRegistry = sessionRegistry;
    }

    public Result addUser(String username, String rawPassword) {
        return addOrEditUser(username, rawPassword, true, false);
    }

    public Result addOrEditUser(String username, String rawPassword, boolean enabled, boolean locked) {
        if(StringUtils.isBlank(username) || StringUtils.isBlank(rawPassword)) {
            return Result.newResult("Username or password is blank");
        }
        User user;
        String result;
        User sameUser = getUserWithUsername(username);
        if(sameUser == null) {
            //new user
            user = User.newUser(username, passwordEncoder.encode(rawPassword));
            user.setEnabled(enabled);
            user.setLocked(locked);
            result = C.CREATED;
        } else {
            //update
            user = sameUser;
            user.setPassword(passwordEncoder.encode(rawPassword));

            if(!enabled && lastEnabledUser()) {
                return Result.newResult("Cannot disable last enabled user");
            } else {
                user.setEnabled(enabled);
            }

            if(!locked && lastUnlockedUser()) {
                return Result.newResult("Cannot lock last unlocked user");
            } else {
                user.setLocked(locked);
            }
            result = C.UPDATED;
        }

        try {
            user = userDao.save(user);
            if(result.equals(C.UPDATED)) {
                if(! user.isEnabled() || user.isLocked()) { forceUserLogout(user); }
            }
            return Result.newResult(result).withPayload(user);
        }catch (Exception e) {
            return Result.newResult(String.format("Unexpected error: %s", e.getMessage()));
        }
    }

    public boolean lastUser() {
        return usersQty()  == 1;
    }

    public boolean lastEnabledUser() {
        List<User> enabledUsers = userDao.findByEnabled(true);
        return enabledUsers.size() == 1;
    }

    public boolean lastUnlockedUser() {
        List<User> unlockedUsers = userDao.findByLocked(false);
        return unlockedUsers.size() == 1;
    }

    public long usersQty() {
        return userDao.count();
    }

    public List<User> getAllUsers() {
        return userDao.findAll();
    }

    public User getUserWithUsername(String username) {
        return userDao.findFirstByUsername(username);
    }

    @SuppressWarnings("RedundantIfStatement")
    public boolean canDelete(User user) {
        if(user == null) return false;
        if(lastUser()) return false;
        if(lastEnabledUser()) return false;
        if(lastUnlockedUser()) return false;

        return true;
    }

    public String deleteUser(User user) {
        if(canDelete(user)) {
            forceUserLogout(user);
            userDao.delete(user);
            return C.SUCCESS;
        } else {
            return "User cannot be deleted";
        }
    }

    private void forceUserLogout(User user) {
        List<SessionInformation> allActiveSessions = sessionRegistry.getAllSessions(new AppUserDetails(user),
                false);
        for(SessionInformation s: allActiveSessions) {
            s.expireNow();
        }
    }
}
