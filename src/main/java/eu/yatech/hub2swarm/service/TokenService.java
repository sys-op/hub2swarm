package eu.yatech.hub2swarm.service;

import eu.yatech.hub2swarm.dao.CredDao;
import eu.yatech.hub2swarm.dao.TokenDao;
import eu.yatech.hub2swarm.model.Cred;
import eu.yatech.hub2swarm.model.Token;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class TokenService {
    private final TextEncryptor encryptor;
    private final TokenDao tokenDao;
    private final CredDao credDao;


    public TokenService(TextEncryptor encryptor,TokenDao tokenDao, CredDao credDao) {
        this.encryptor = encryptor;
        this.tokenDao = tokenDao;
        this.credDao = credDao;
    }

    public Result addToken(String plainToken, Cred creds) {
        if(StringUtils.isAllEmpty(plainToken)) {
            return Result.newResult("Got empty token");
        }

        if(! credDao.existsById(creds.getId())) {
            return Result.newResult("Given creds doesn't exist");
        }

        String encryptedToken = encryptor.encrypt(plainToken);
        long expirationTime = System.currentTimeMillis() + C.TOKEN_LIFE;

        try {
            Token token = Token.newToken(encryptedToken, expirationTime, creds);
            token = tokenDao.save(token);
            return Result.newResult(C.CREATED).withPayload(token);
        }catch (Exception e) {
            return Result.newResult(String.format("Unexpected error: %s", e.getMessage()));
        }
    }

    public List<Token> getActiveTokensByCreds(Cred cred) {
        List<Token> allTokens = tokenDao.findByCred(cred);

        if(allTokens.isEmpty()) return allTokens;

        List<Token> activeTokens = new ArrayList<>();
        for (Token t: allTokens) {
            long expirationTime = t.getExpirationTime();
            if(expirationTime > System.currentTimeMillis()) {
                activeTokens.add(t);
            } else {
                delete(t);
            }
        }
        return activeTokens;
    }

    private void delete(Token token) {
        tokenDao.delete(token);
    }
}
