package eu.yatech.hub2swarm.service;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import eu.yatech.hub2swarm.dao.ImageDao;
import eu.yatech.hub2swarm.dao.StackDao;
import eu.yatech.hub2swarm.model.DockerImage;
import eu.yatech.hub2swarm.model.Stack;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImageService {
    private final ImageDao imageDao;
    private final StackDao stackDao;

    public ImageService(ImageDao imageDao, StackDao stackDao) {
        this.imageDao = imageDao;
        this.stackDao = stackDao;
    }

    public Result addOrEditDockerImage(String repo, String tag) {
        DockerImage image;
        String result;

        //Update current
        DockerImage imagesWithSameRepoAndTag = imageDao.findFirstByRepoAndTag(repo, tag);
        if(imagesWithSameRepoAndTag != null) {
            image = imagesWithSameRepoAndTag;
            result = C.UPDATED;
        } else {
            image = DockerImage.newImage(repo, tag);
            result = C.CREATED;
        }

        try {
            DockerImage savedImage = imageDao.save(image);
            return Result.newResult(result).withPayload(savedImage);
        } catch (DataIntegrityViolationException e) {
            String message = "Image with this repo or tag already registered. Choose another or update existing";
            return Result.newResult(message);
        } catch (Exception e) {
            String message = String.format("Unexpected error: %s", e.getMessage());
            return Result.newResult(message);
        }

    }

    public long imagesQty() {
        return imageDao.count();
    }

    public List<DockerImage> getAllImages() {
        return imageDao.findAll();
    }

    public boolean canDelete(DockerImage image) {
        if(image == null) return false;
        if(! imageDao.existsById(image.getId())) {
            return false;
        }

        return stackDao.findByImage(image).isEmpty();
    }

    public String deleteImage(DockerImage image) {
        if (canDelete(image)) {
            try {
                imageDao.delete(image);
                return C.SUCCESS;
            } catch (ConstraintViolationException c) {
                return String.format("Something is still blocking %s from been deleted", image.toString());
            } catch (Exception e) {
                return String.format("Unexpected error: %s", e.getMessage());
            }
        } else {
            if(image == null)  return "Nothing to delete";
            return String.format("%s cannot be deleted. " +
                    "Some services or credentials are linked to this swarm or no such image found", image.toString());
        }
    }

    public DockerImage getImageFromString(String repoTagString) {
        if (repoTagString == null) return null;

        String[] repoTag = repoTagString.split(":");
        if (repoTag.length < 2) {
            return null;
        } else {
            return getImageByRepoAndTag(repoTag[0], repoTag[1]);
        }
    }

    public DockerImage getImageByRepoAndTag(String repo, String tag) {
        return imageDao.findFirstByRepoAndTag(repo, tag);
    }

    public Icon mappingStatus(DockerImage image) {
        List<Stack> stacks = stackDao.findByImage(image);
        return stacks.isEmpty() ? new Icon(VaadinIcon.WARNING) : new Icon(VaadinIcon.CHECK);
    }

    public Text listStacksByImage(DockerImage image) {
        List<Stack> stacks = stackDao.findByImage(image);
        if(stacks.isEmpty()) {
            return new Text("-");
        }
        List<String> stackNames = new ArrayList<>();
        for(Stack stack: stacks) {
            stackNames.add(stack.getStackName());
        }
        return new Text(stackNames.toString());
    }

    public DockerImage findImageByString(String imageAsString) {
        if (StringUtils.isBlank(imageAsString)) return null;
        String[] parts = imageAsString.trim().split(":");
        if (parts.length > 1) {
            return imageDao.findFirstByRepoAndTag(parts[0], parts[1]);
        } else {
            return null;
        }
    }

}
