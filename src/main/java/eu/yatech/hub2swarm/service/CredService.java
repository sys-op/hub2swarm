package eu.yatech.hub2swarm.service;

import eu.yatech.hub2swarm.dao.CredDao;
import eu.yatech.hub2swarm.dao.SwarmDao;
import eu.yatech.hub2swarm.dao.TokenDao;
import eu.yatech.hub2swarm.model.Cred;
import eu.yatech.hub2swarm.model.Swarm;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CredService {
    private final CredDao credDao;
    private final SwarmDao swarmDao;
    private final TokenDao tokenDao;

    public CredService(CredDao credDao, SwarmDao swarmDao, TokenDao tokenDao) {
        this.credDao = credDao;
        this.swarmDao = swarmDao;
        this.tokenDao = tokenDao;
    }

    public Result addOrEditCred(String username, String encryptedPassword, Swarm swarm) {
        Cred cred = null;
        String result;

        if(StringUtils.isBlank(username)) {
            return Result.newResult("Username: cannot be empty");
        }

        if(StringUtils.isBlank(encryptedPassword)) {
            return Result.newResult("Password: cannot be empty");
        }

        if(swarmDao.findFirstByLabel(swarm.getLabel()) == null) {
            return Result.newResult("No such Swarm found");
        }

        List<Cred> credsInSameSwarm = credDao.findBySwarm(swarm);
        if(! credsInSameSwarm.isEmpty()) {
            for(Cred c: credsInSameSwarm) {
                if(c.getUsername().equals(username)) {
                    cred = c;
                    break;
                }
            }
        } //swarm has no creds - creating one

        if(cred == null) {
            cred = Cred.newCred(username, encryptedPassword, swarm);
            result = C.CREATED;
        } else {
            cred.setUsername(username);
            cred.setPassword(encryptedPassword);
            cred.setSwarm(swarm);
            result = C.UPDATED;
        }

        try {
            Cred savedCreds = credDao.save(cred);
            return Result.newResult(result).withPayload(savedCreds);
        } catch (Exception e) {
            String message = String.format("Unexpected error: %s", e.getMessage());
            return Result.newResult(message);
        }
    }

    public long credQty() {
        return credDao.count();
    }

    public List<Cred> getAllCreds() {
        return credDao.findAll();
    }

    public boolean canDelete(Cred cred) {
        if(cred == null) return false;
        if(credDao.existsById(cred.getId())) {
            return tokenDao.findByCred(cred).isEmpty();
        } else {
            return false;
        }
    }

    public String deleteCred(Cred cred) {
        if(cred == null) return "Nothing to delete";

        if(canDelete(cred)) {
            try{
                credDao.delete(cred);
                return C.SUCCESS;
            } catch (Exception e) {
                return String.format("Unexpected error: %s", e.getMessage());
            }
        } else {
            return String.format("%s cannot be deleted", cred.toString());
        }
    }

    public Cred getCredByUsernameAndSwarm(String username, Swarm swarm) {
        return credDao.findByUsernameAndSwarm(username, swarm);
    }

    public Cred credBySwarm(Swarm swarm) {
        //get creds by swarm
        List<Cred> creds = credDao.findBySwarm(swarm);
        if (creds.isEmpty()) {
            return null;
        }
        //here we use only first creds, because I am lazy.
        return creds.get(0);
    }

}
