package eu.yatech.hub2swarm.service;

import eu.yatech.hub2swarm.app.ImageReplacePolicy;
import eu.yatech.hub2swarm.dao.ImageDao;
import eu.yatech.hub2swarm.dao.StackDao;
import eu.yatech.hub2swarm.dao.SwarmDao;
import eu.yatech.hub2swarm.model.DockerImage;
import eu.yatech.hub2swarm.model.Stack;
import eu.yatech.hub2swarm.model.Swarm;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StackService {
    private final StackDao stackDao;
    private final SwarmDao swarmDao;
    private final ImageDao imageDao;

    public StackService(StackDao stackDao, SwarmDao swarmDao, ImageDao imageDao) {
        this.stackDao = stackDao;
        this.swarmDao = swarmDao;
        this.imageDao = imageDao;
    }

    public Result addOrEditStack(String stackName, int stackId, Swarm swarm, DockerImage image,
                                 ImageReplacePolicy imageReplacePolicy) {
        String result;

        if(StringUtils.isBlank(stackName)) {
            return Result.newResult("Stack Name: cannot be empty");
        }

        if(stackId <= 0) {
            return Result.newResult("Stack ID: must be possible integer");
        }

        if(swarmDao.findFirstByLabel(swarm.getLabel()) == null) {
            return Result.newResult("No such Swarm found");
        }
        if(imageDao.findFirstByRepoAndTag(image.getRepo(), image.getTag())== null) {
            return Result.newResult("No such Docker image found");
        }

        Stack sameStack = stackDao.findFirstByStackNameAndSwarmAndImage(stackName, swarm, image);
        Stack stack;
        if(sameStack == null) {
            stack = Stack.newStack(stackName, stackId, swarm, image, imageReplacePolicy.getValue());
            result = C.CREATED;
        } else {
            stack = sameStack;
            stack.setImageReplacePolicy(imageReplacePolicy.getValue());
            result = C.UPDATED;
        }

        try {
            Stack savedStack = stackDao.save(stack);
            return Result.newResult(result).withPayload(savedStack);
        } catch (Exception e) {
            String message = String.format("Unexpected error: %s", e.getMessage());
            return Result.newResult(message);
        }
    }

    public long stackQty() {
        return stackDao.count();
    }

    public List<Stack> getAllStacks() {
        return stackDao.findAll();
    }

    public boolean canDelete(Stack stack) {
        if(stack == null) return false;
        //if exists - can be deleted
        return stackDao.existsById(stack.getId());
    }

    public String deleteStack(Stack stack) {
        if(stack == null)  return "Nothing to delete";

        if (canDelete(stack)) {
            try {
                stackDao.delete(stack);
                return C.SUCCESS;
            } catch (ConstraintViolationException c) {
                return String.format("Something is still blocking %s from been deleted", stack.toString());
            } catch (Exception e) {
                return String.format("Unexpected error: %s", e.getMessage());
            }
        } else {
            return String.format("%s cannot be deleted", stack.toString());
        }
    }

    public Stack getStackByNameAndSwarm(String stackName, Swarm swarm, DockerImage image) {
        return stackDao.findFirstByStackNameAndSwarmAndImage(stackName, swarm, image);
    }

}
