package eu.yatech.hub2swarm.service;

import eu.yatech.hub2swarm.dao.CredDao;
import eu.yatech.hub2swarm.dao.StackDao;
import eu.yatech.hub2swarm.dao.SwarmDao;
import eu.yatech.hub2swarm.model.Swarm;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SwarmService {
    private final SwarmDao swarmDao;
    private final StackDao stackDao;
    private final CredDao credDao;

    public SwarmService(SwarmDao swarmDao, StackDao stackDao, CredDao credDao) {
        this.swarmDao = swarmDao;
        this.stackDao = stackDao;
        this.credDao = credDao;
    }

    public Result addOrEditSwarm(String label, String url) {
        Swarm swarm;
        String result;

        //Update current
        Swarm swarmWithSameLabel = swarmDao.findFirstByLabel(label);
        Swarm swarmWithSameUrl = swarmDao.findFirstByUrl(url);
        if(swarmWithSameLabel != null) {
            swarm = swarmWithSameLabel;
            swarm.setUrl(url);
            result = C.UPDATED;
        } else if (swarmWithSameUrl != null) {
            swarm = swarmWithSameUrl;
            swarm.setLabel(label);
            result = C.UPDATED;
        } else {
            //Or create new
            swarm = Swarm.newSwarm(label, url);
            result = C.CREATED;
        }

        try {
            Swarm savedSwarm = swarmDao.save(swarm);
            return Result.newResult(result).withPayload(savedSwarm);
        } catch (DataIntegrityViolationException e) {
            String message = "Swarm with this label or URL already registered. Choose another label or update existing";
            return Result.newResult(message);
        } catch (Exception e) {
            String message = String.format("Unexpected error: %s", e.getMessage());
            return Result.newResult(message);
        }

    }

    public long swarmsQty() {
        return swarmDao.count();
    }

    public List<Swarm> getAllSwarms() {
        return swarmDao.findAll();
    }

    public boolean canDelete(Swarm swarm) {
        if(swarm == null) return false;
        if(! swarmDao.existsById(swarm.getId())) {
            return false;
        }

        if(stackDao.findBySwarm(swarm).size() > 0) {
            return false;
        } else {
            return credDao.findBySwarm(swarm).isEmpty();
        }
    }

    public String deleteSwarm(Swarm swarm) {
        if (canDelete(swarm)) {
            try {
                swarmDao.delete(swarm);
                return C.SUCCESS;
            } catch (ConstraintViolationException c) {
                return String.format("Something is still blocking %s from been deleted", swarm.toString());
            } catch (Exception e) {
                return String.format("Unexpected error: %s", e.getMessage());
            }
        } else {
            if(swarm == null)  return "Nothing to delete";
            return String.format("%s cannot be deleted. " +
                    "Some services or credentials are linked to this swarm or no such swarm found", swarm.toString());
        }
    }

    public Swarm getSwarmByLabel(String label) {
        return swarmDao.findFirstByLabel(label);
    }
}
