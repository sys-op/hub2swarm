package eu.yatech.hub2swarm.service;

import eu.yatech.hub2swarm.app.DeploymentStatus;
import eu.yatech.hub2swarm.dao.DeploymentDao;
import eu.yatech.hub2swarm.model.Deployment;
import eu.yatech.hub2swarm.model.DockerImage;
import eu.yatech.hub2swarm.utils.Broadcaster;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import eu.yatech.hub2swarm.utils.ThreadNamer;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class DeploymentService {

    private final DeploymentDao deploymentDao;

    public DeploymentService(DeploymentDao deploymentDao) {
        this.deploymentDao = deploymentDao;
    }

    public Result addSuccessfulDeployment(String message, DockerImage image) {
        return addDeployment(DeploymentStatus.OK, message, image);
    }

    public Result addFailedDeployment(String message, DockerImage image) {
        return addDeployment(DeploymentStatus.FAIL, message, image);
    }

    private Result addDeployment(DeploymentStatus status, String message, DockerImage image) {
        if (Objects.isNull(status) || StringUtils.isBlank(message)) {
            return Result.newResult("Message: must be not empty string");
        }

        String deploymentIdStr = Thread.currentThread().getName();
        if (StringUtils.isBlank(deploymentIdStr)) {
            return Result.newResult("Unable to get current Thread name");
        }
        String deploymentId = deploymentIdStr.trim().replaceFirst(ThreadNamer.DEPLOYMENT_PREFIX, "");

        List<Deployment> deploymentsWithSameName = deploymentDao.findByDeploymentId(deploymentId);
        if (!deploymentsWithSameName.isEmpty()) {
            deploymentId = deploymentId + "-1"; //this is not ideal, but in current situation enough
        }

        Deployment deployment = Deployment.newDeployment(deploymentId, image.toDockerImage(), status.getValue(), message);

        try {
            Deployment savedDeployment = deploymentDao.save(deployment);
            return Result.newResult(C.CREATED).withPayload(savedDeployment);
        } catch (Exception e) {
            String exMessage = String.format("Unexpected error: %s", e.getMessage());
            return Result.newResult(exMessage);
        } finally {
            Broadcaster.broadcast("");
        }
    }

    public long deploymentsQty() {
        return deploymentDao.count();
    }

    public List<Deployment> getAllDeployments() {
        return deploymentDao.findAllByOrderByCreatedDesc();
    }

    public List<Deployment> getAllWithStatus(DeploymentStatus status) {
        return deploymentDao.findByStatusOrderByCreatedDesc(status.getValue());
    }

    public Deployment getByDeploymentId(String deploymentId) {
        return deploymentDao.findFirstByDeploymentId(deploymentId);
    }

}
