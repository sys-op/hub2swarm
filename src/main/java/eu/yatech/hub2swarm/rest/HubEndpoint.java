package eu.yatech.hub2swarm.rest;

import eu.yatech.hub2swarm.app.DockerHubParser;
import eu.yatech.hub2swarm.app.Updater;
import eu.yatech.hub2swarm.model.DockerImage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;

@Slf4j
@RestController
public class HubEndpoint {
    private final DockerHubParser hubParser;
    private final Updater updater;

    public HubEndpoint(DockerHubParser parser, Updater updater) {
        hubParser = parser;
        this.updater = updater;
    }

    @PostMapping(value = "/hub", produces = "application/json")
    public ResponseEntity<Object> fromDockerHub(@RequestBody String body) {
        log.info("Got new request from Docker Hub");
        log.debug("Raw request: {}", body);

        DockerImage dataFromHub;
        try {
            dataFromHub = hubParser.hubJson2Image(body);
        } catch (ParseException e) {
            String message = e.getMessage() + ". JSON: "+body;
            log.error(message, e);
            return ResponseEntity.status(500).body(message);
        }

        //showing output to logs
        log.info("{} was uploaded to DockerHub", dataFromHub.toDockerImage());
        updater.doUpdate(dataFromHub);
        return ResponseEntity.ok(dataFromHub);
    }

}
