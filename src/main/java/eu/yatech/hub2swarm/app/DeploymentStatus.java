package eu.yatech.hub2swarm.app;

import com.vaadin.flow.component.icon.VaadinIcon;

public enum DeploymentStatus {
    OK(new Byte("0"), VaadinIcon.SUN_O),
    FAIL(new Byte("1"), VaadinIcon.FIRE);

    private final byte value;
    private final VaadinIcon icon;

    DeploymentStatus(byte status, VaadinIcon icon) {
        this.value = status;
        this.icon = icon;
    }

    public byte getValue() {
        return this.value;
    }

    public VaadinIcon getIcon() {
        return this.icon;
    }
}
