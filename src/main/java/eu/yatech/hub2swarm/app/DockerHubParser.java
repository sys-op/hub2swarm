package eu.yatech.hub2swarm.app;

import eu.yatech.hub2swarm.model.DockerImage;
import org.springframework.stereotype.Service;

import javax.json.Json;
import javax.json.JsonException;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.text.ParseException;
import java.util.Objects;

import static eu.yatech.hub2swarm.utils.C.NO_VALUE_MARKER;

@Service
public class DockerHubParser {
    public DockerImage hubJson2Image(String body) throws ParseException {
        String tag;
        String repoName;
        JsonObject json;

        JsonReader jsonReader = Json.createReader(new StringReader(body));
        try {
            json = jsonReader.readObject();
        } catch (JsonException e) {
            throw new ParseException("Unparseable JSON. Message: "+e.getMessage(), 0);
        }

        JsonObject repository = json.getJsonObject("repository");
        if (Objects.nonNull(repository)) {
            repoName = repository.getString("repo_name", NO_VALUE_MARKER);
        } else {
            repoName = NO_VALUE_MARKER;
        }

        JsonObject pushData = json.getJsonObject("push_data");
        if(Objects.nonNull(pushData)) {
            tag = pushData.getString("tag", NO_VALUE_MARKER);
        } else {
            tag = NO_VALUE_MARKER;
        }


        return DockerImage.newImage(repoName, tag);
    }
}
