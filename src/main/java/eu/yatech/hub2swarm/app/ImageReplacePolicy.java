package eu.yatech.hub2swarm.app;

public enum ImageReplacePolicy {
    NO_REPLACE(new Byte("0")),
    REPLACE(new Byte("1"));

    private final byte value;
    ImageReplacePolicy(byte policy) {
        this.value = policy;
    }

    public byte getValue() {
        return this.value;
    }

}
