package eu.yatech.hub2swarm.app;

import eu.yatech.hub2swarm.dao.StackDao;
import eu.yatech.hub2swarm.model.DockerImage;
import eu.yatech.hub2swarm.model.Stack;
import eu.yatech.hub2swarm.service.DeploymentService;
import eu.yatech.hub2swarm.service.ImageService;
import eu.yatech.hub2swarm.utils.Broadcaster;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import eu.yatech.hub2swarm.utils.ThreadNamer;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Slf4j
public class Updater {
    private final StackDao stackDao;

    private final ImageService imageService;
    private final DeploymentService deploymentService;

    private final Deployer deployer;

    public Updater(ImageService imageService, StackDao stackDao, DeploymentService deploymentService,
                   Deployer deployer) {
        this.imageService = imageService;
        this.stackDao = stackDao;
        this.deploymentService = deploymentService;
        this.deployer = deployer;
    }

    public void doUpdate(String imageAsString) {
        doUpdate(imageService.getImageFromString(imageAsString));
    }

    @Async
    public void doUpdate(DockerImage parsedImage) {
        Thread.currentThread().setName(ThreadNamer.deployerName());

        String deploymentMessage = "-";
        boolean alreadyDeployedMutex = false;

        try {
            log.info("Image to update: {}", parsedImage.toDockerImage());

            //searching image with same repo+tag
            DockerImage foundImage = imageService.getImageByRepoAndTag(parsedImage.getRepo(), parsedImage.getTag());
            if (foundImage != null) {
                log.info("searching image with same repo+tag....Found!");
            } else {
                //Do automagic, if on
                log.info("searching image with same repo+tag....Not found: Doing image automagic, if on");
                Result automagicResult = imageAutomagic(parsedImage);
                if (automagicResult.getMessage().equals(C.CREATED) || automagicResult.getMessage().equals(C.UPDATED)) {
                    foundImage = (DockerImage) automagicResult.getPayload();
                } else {
                    throw new RuntimeException(automagicResult.getMessage());
                }
            }

            //Find mapping
            List<Stack> stacksToDeploy = stackDao.findByImage(foundImage);
            if (stacksToDeploy.isEmpty()) {
                throw new NoSuchElementException(String.format("Nowhere to deploy. No Swarms yet mapped to %s",
                        foundImage.toDockerImage()));
            }
            for (Stack stack : stacksToDeploy) {
                deploymentMessage = deployer.doDeploy(stack);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            Result result = deploymentService.addFailedDeployment(e.getMessage(), parsedImage);
            alreadyDeployedMutex = true;
            logIfFail(result);
        } finally {
            if (!alreadyDeployedMutex) {
                Result result = deploymentService.addSuccessfulDeployment(deploymentMessage, parsedImage);
                logIfFail(result);
            }
        }
    }

    private Result imageAutomagic(DockerImage image) {
        boolean automagicEnabled = automagicEnabled();
        if (automagicEnabled) {
            //automagically adding parsed image to table
            Result result = imageService.addOrEditDockerImage(image.getRepo(), image.getTag());
            if (result.getMessage().equals(C.CREATED) || result.getMessage().equals(C.UPDATED)) {
                log.info("Automagically adding {}", image.toDockerImage());
                //Announcing DB update
                Broadcaster.broadcast("Database Updated");
                return result;
            } else {
                log.error("Image Automagic failed. Error: {}", result.getMessage());
                return Result.newResult("Automagic fail");
            }
        } else {
            return Result.newResult("Automagic disabled and no image found in Database. Nothing to deploy");
        }
    }

    private boolean automagicEnabled() {
        String imageAutomagic;
        String imageAutomagicEnv = System.getenv(C.HUB_AUTOMAGIC_ENV);
        if (StringUtils.isBlank(imageAutomagicEnv)) {
            imageAutomagic = C.ON;
        } else {
            if (imageAutomagicEnv.toUpperCase().equals(C.OFF)) {
                imageAutomagic = C.OFF;
            } else {
                imageAutomagic = C.ON;
            }
        }
        return imageAutomagic.equals(C.ON);
    }

    private void logIfFail(Result result) {
        if (!result.getMessage().equals(C.CREATED)) {
            log.error(result.getMessage());
        }
    }
}
