package eu.yatech.hub2swarm.app;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import eu.yatech.hub2swarm.json.AuthJson;
import eu.yatech.hub2swarm.json.StackInfo;
import eu.yatech.hub2swarm.model.*;
import eu.yatech.hub2swarm.service.CredService;
import eu.yatech.hub2swarm.service.ImageService;
import eu.yatech.hub2swarm.service.StackService;
import eu.yatech.hub2swarm.service.TokenService;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import javassist.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.stereotype.Service;

import javax.security.auth.login.LoginException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Slf4j
public class Deployer {
    private final TextEncryptor encryptor;
    private final TokenService tokenService;
    private final CredService credService;
    private final ImageService imageService;
    private final StackService stackService;

    private String token;

    public Deployer(TextEncryptor encryptor, TokenService tokenService, CredService credService,
                    ImageService imageService, StackService stackService) {
        this.encryptor = encryptor;
        this.tokenService = tokenService;
        this.credService = credService;
        this.imageService = imageService;
        this.stackService = stackService;
    }

    String doDeploy(Stack stack) throws Exception {
        if (stack == null) return "Stack: cannot be NULL";

        //Find swarm to go
        Swarm swarm = stack.getSwarm();
        log.info("Starting deploy {} to {}", stack.getImage().toDockerImage(), swarm.getLabel());

        this.token = searchToken(swarm);
        if (this.token.equals(C.NO_VALUE_MARKER)) {
            //no tokens - have to login
            this.token = doLogin(swarm);
        }
        StackInfo stackInfo = getStackInfo(stack);
        addStackFile(stack, stackInfo);

        DockerImage currentlyDeployedImage = getCurrentlyDeployedImage(stackInfo.getStackFile());
        if (currentlyDeployedImage != null && currentlyDeployedImage != stack.getImage()) {
            ImageReplacePolicy replacePolicyOfCurrentlyDeployedImage =
                    getReplacePolicyOfCurrentlyDeployedImage(currentlyDeployedImage, stack);

            if (replacePolicyOfCurrentlyDeployedImage == ImageReplacePolicy.REPLACE) {
                stackInfo.setStackFile(replaceImageInStackFile(stack.getImage(), stackInfo.getStackFile()));
            } else {
                log.error("Deploy denied. Forbidden to change image from '{}' to '{}'",
                        currentlyDeployedImage.toDockerImage(), stack.getImage().toDockerImage());
                log.error("Change imageReplacePolicy for {} to allow", currentlyDeployedImage.toDockerImage());

                return String.format("Deploy denied. Forbidden to change image from '%s' to '%s'",
                        currentlyDeployedImage.toDockerImage(), stack.getImage().toDockerImage());
            }
        }
        return doStackUpdate(stack, stackInfo);
    }

    private String searchToken(Swarm swarm) throws NotFoundException {
        //maybe be we already have tokens
        Cred swarmCreds = credentialsBySwarm(swarm);
        List<Token> activeTokens = tokenService.getActiveTokensByCreds(swarmCreds);

        if (activeTokens.isEmpty()) {
            log.info("There no active tokens for {} - we have to login", swarm.getLabel());
            //no tokens - have to login
            return C.NO_VALUE_MARKER;
        } else {
            //using first
            log.info("Found active token for {} - reusing it", swarm.getLabel());
            String token = activeTokens.get(0).getToken();
            return decryptToken(token);
        }
    }

    private String doLogin(Swarm swarm) throws LoginException, NotFoundException {
        log.info("Logging in to {}", swarm.getLabel());
        Cred swarmCreds = credentialsBySwarm(swarm);

        String swarmUrl = String.format("%s/api/auth", swarm.getUrl());
        String username = swarmCreds.getUsername();

        String password;
        try {
            password = encryptor.decrypt(swarmCreds.getPassword());
        } catch (Exception e) {
            throw new LoginException("Unable to decrypt Portainer password");
        }

        String token;
        try {
            AuthJson authJson = AuthJson.newAuth(username, password);
            HttpResponse<JsonNode> loginResponse = Unirest.post(swarmUrl)
                    .header("Content-Type", "application/json")
                    .body(authJson.toJson())
                    .asJson();

            switch (loginResponse.getStatus()) {
                case 200:
                    break;
                case 400:
                case 500:
                case 503:
                    throw new LoginException(String.format("Login failed. Status: %d, Response: %s",
                            loginResponse.getStatus(), loginResponse.getBody().getObject().getString("err")));
                case 404:
                    throw new LoginException(String.format("Endpoint %s is unreachable", swarmUrl));
            }

            JsonNode payload = loginResponse.getBody();
            token = payload.getObject().getString("jwt");
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new LoginException("Got exception while logging in");
        }

        //got token
        log.info("Logged in. We have token");
        saveTokenToDb(token, swarmCreds);
        return token;
    }

    private StackInfo getStackInfo(Stack stack) throws UnirestException {
        if(StringUtils.isBlank(this.token)) {
            throw new RuntimeException("Found empty token. Cannot make requests to Portainer's protected endpoints");
        }
        String endpointUrl = String.format("%s/api/stacks/%d", stack.getSwarm().getUrl(), stack.getStackId());
        HttpResponse<JsonNode> stackInfoResp = Unirest.get(endpointUrl)
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer "+ this.token)
                .asJson();

        switch (stackInfoResp.getStatus()) {
            case 200:
                break;
            case 400:
            case 403:
            case 404:
            case 500:
                throw new RuntimeException(String.format("Login failed. Status: %d, Response: %s",
                        stackInfoResp.getStatus(), stackInfoResp.getBody().getObject().getString("err")));
        }
        JsonNode json = stackInfoResp.getBody();

        String name = json.getObject().getString("Name");
        int endpointId = json.getObject().getInt("EndpointId");
        JSONArray env = json.getObject().getJSONArray("Env");
        return StackInfo.newStackInfo(name, endpointId, env);
    }

    private void addStackFile(Stack stack, StackInfo stackInfo) throws UnirestException {
        String endpointUrl = String.format("%s/api/stacks/%d/file", stack.getSwarm().getUrl(), stack.getStackId());
        HttpResponse<JsonNode> stackFileResp = Unirest.get(endpointUrl)
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer "+ this.token)
                .asJson();
        switch (stackFileResp.getStatus()) {
            case 200:
                break;
            case 400:
            case 403:
            case 404:
            case 500:
                throw new RuntimeException(String.format("Login failed. Status: %d, Response: %s",
                        stackFileResp.getStatus(), stackFileResp.getBody().getObject().getString("err")));
        }

        JsonNode json = stackFileResp.getBody();
        stackInfo.setStackFile(json.getObject().getString("StackFileContent"));
    }

    private DockerImage getCurrentlyDeployedImage(String stackFile) {
        //grep image
        Pattern p = Pattern.compile("image: .+\n");
        Matcher m = p.matcher(stackFile);
        String currentImageStr;
        if (m.find()) {
            currentImageStr = m.group().replaceFirst("image:", "").trim();
        } else {
            log.error("Damaged stack file");
            return null;
        }
        return imageService.findImageByString(currentImageStr);
    }

    private ImageReplacePolicy getReplacePolicyOfCurrentlyDeployedImage(DockerImage currentImage, Stack stack) {
        Stack currentlyDeployedStack =
                stackService.getStackByNameAndSwarm(stack.getStackName(), stack.getSwarm(), currentImage);

        if (currentlyDeployedStack != null) {
            //return its policy
            return currentlyDeployedStack.getReplacePolicy();
        } else {
            log.error("Currently deployed Stack with image: {} not found in our DB. " +
                    "Therefore replace policy is undefined. It is safe to deny update", currentImage.toDockerImage());
            return ImageReplacePolicy.NO_REPLACE;
        }
    }

    private String replaceImageInStackFile(DockerImage image, String stackFile) {
        return stackFile.replaceFirst("image: .+\n","image: " + image.toDockerImage()+"\n");

    }

    private String doStackUpdate(Stack stack, StackInfo stackInfo) throws UnirestException {
        String endpointUrl = String.format("%s/api/stacks/%d?endpointId=%d", stack.getSwarm().getUrl(),
                stack.getStackId(), stackInfo.getEndpointId());
        HttpResponse<JsonNode> updateResponse = Unirest.put(endpointUrl)
                .header("Content-Type", "application/json")
                .header("Authorization", "Bearer "+ this.token)
                .body(stackInfo.toUpdateJson())
                .asJson();

        switch (updateResponse.getStatus()) {
            case 200:
                String wordOfSuccess = String.format("Stack '%s@%s' was updated. Deployed image: '%s'",
                        stack.getStackName(), stack.getSwarm().getLabel(), stack.getImage().toDockerImage());

                log.info(wordOfSuccess);
                return wordOfSuccess;
            case 400:
            case 403:
            case 404:
            case 500:
                throw new RuntimeException(String.format("Login failed. Status: %d, Response: %s",
                        updateResponse.getStatus(), updateResponse.getBody().getObject().getString("err")));
            default:
                throw new RuntimeException(String.format("Got unknown status code %s", updateResponse.getStatus()));
        }
    }

    //Very private functions
    private Cred credentialsBySwarm(Swarm swarm) throws NotFoundException {
        Cred credentials = credService.credBySwarm(swarm);
        if (credentials == null) {
            throw new NotFoundException("No credentials found for " + swarm);
        }
        return credentials;
    }

    private String decryptToken(String encryptedToken) {
        String token;
        try {
            token = encryptor.decrypt(encryptedToken);
        } catch (Exception e) {
            throw new RuntimeException("Failed to decrypt Portainer access token");
        }
        return token;
    }

    private void saveTokenToDb(String token, Cred swarmCreds) {
        Result result = tokenService.addToken(token, swarmCreds);
        if (!result.getMessage().equals(C.CREATED)) {
            log.error("Failed to save token to DB" + result.getMessage());
        }
    }
}
