package eu.yatech.hub2swarm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hub2swarmApplication {

	public static void main(String[] args) {
		SpringApplication.run(Hub2swarmApplication.class, args);
	}

}
