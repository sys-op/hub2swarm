package eu.yatech.hub2swarm.config;

import eu.yatech.hub2swarm.utils.C;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.buf.HexUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

@Slf4j
@Configuration
public class Encryption {

    private Environment applicationProperties;

    @Autowired
    @Bean
    public TextEncryptor appEncryptor(Environment props) {
        applicationProperties = props;
        CharSequence secretKeyPassword = getSecretKeyPassword();
        CharSequence salt = getSalt();

        return Encryptors.delux(secretKeyPassword, salt);
    }

    private CharSequence getSecretKeyPassword() {
        CharSequence password;
        CharSequence passFromEnv = System.getenv(C.SECRET_KEY_PASSWORD_ENV);
        if(passFromEnv == null || passFromEnv.length() == 0) {
            CharSequence passFromProps = applicationProperties.getProperty(C.SECRET_KEY_PASSWORD_PROP, C.NO_VALUE_MARKER);
            if(passFromProps == C.NO_VALUE_MARKER) {
                password = "";
                log.error("Encryptor ERROR: No secret key password found in ENV '{}' nor application property '{}'",
                        C.SECRET_KEY_PASSWORD_ENV, C.SECRET_KEY_PASSWORD_PROP);
                System.exit(C.APPLICATION_NOT_SECURE);
            } else {
                password = passFromProps;
            }
        } else {
            password = passFromEnv;
        }
        return password;
    }

    private CharSequence getSalt() {
        CharSequence salt;
        CharSequence saltFromEnv = System.getenv(C.SALT_ENV);
        if(saltFromEnv == null || saltFromEnv.length()==0) {
            CharSequence saltFromProps = applicationProperties.getProperty(C.SALT_PROP, C.NO_VALUE_MARKER);
            if(saltFromProps == C.NO_VALUE_MARKER) {
                salt = "";
                log.error("Encryptor ERROR: No salt found in ENV '{}' nor application property '{}'", C.SALT_ENV, C.SALT_PROP);
                System.exit(C.APPLICATION_NOT_SECURE);
            } else {
                salt = saltFromProps;
            }
        } else {
            salt = saltFromEnv;
        }

        //as salt must be HEX encoded - we do it now
        return HexUtils.toHexString(salt.toString().getBytes());
    }
}
