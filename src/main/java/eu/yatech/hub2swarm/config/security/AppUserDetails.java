package eu.yatech.hub2swarm.config.security;

import eu.yatech.hub2swarm.model.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class AppUserDetails implements UserDetails {

    private static final String USE_APP_ROLE = "USE-APP-ROLE";

    private final User user;

    public AppUserDetails(User user) {
        super();
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Collection<GrantedAuthority> result = new ArrayList<>();
        result.add((GrantedAuthority) () -> USE_APP_ROLE);
        return result;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        //we have no expiration logic
        return user.isEnabled();
    }

    @Override
    public boolean isAccountNonLocked() {
        return ! user.isLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //we have no expiration logic
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.isEnabled();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AppUserDetails other = (AppUserDetails) obj;
        if (user.getUsername() == null) {
            return other.user.getUsername() == null;
        } else return user.getUsername().equals(other.getUsername());
    }

}
