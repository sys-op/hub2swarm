package eu.yatech.hub2swarm.config.security;

import eu.yatech.hub2swarm.dao.UserDao;
import eu.yatech.hub2swarm.model.User;
import org.springframework.stereotype.Service;

@Service
public class AuthUserLookUpService {

    private final UserDao userDao; //cannot use UserService because of cycle dependencies

    public AuthUserLookUpService(UserDao userDao) {
        this.userDao = userDao;
    }

    AppUserDetails findUser(String username) {
        AppUserDetails found;
        User user = userDao.findFirstByUsername(username);

        found = (user != null) ? new AppUserDetails(user) : null;
        return found;
    }
}
