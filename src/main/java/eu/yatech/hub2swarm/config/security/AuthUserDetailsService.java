package eu.yatech.hub2swarm.config.security;


import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Qualifier(AuthUserDetailsService.NAME)
public class AuthUserDetailsService implements UserDetailsService {

    public static final String NAME = "UserDetailService";

    private final AuthUserLookUpService userLookupService;

    public AuthUserDetailsService(AuthUserLookUpService userLookupService) {
        this.userLookupService = userLookupService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserDetails user = userLookupService.findUser(username);
        if(Objects.nonNull(user)) {
            return user;
        } else {
            throw new UsernameNotFoundException(String.format("Username '%s': no such user", username));
        }
    }
}
