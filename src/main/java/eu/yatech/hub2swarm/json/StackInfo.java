package eu.yatech.hub2swarm.json;

import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.json.JSONArray;
import org.json.JSONObject;

@RequiredArgsConstructor(staticName = "newStackInfo")
public
@Data class StackInfo {
    @NonNull private final String name;
    @NonNull private final int endpointId;
    @NonNull private final JSONArray env;

    private String stackFile;

    public JSONObject toUpdateJson() {
        JSONObject updateJson = new JSONObject();
        updateJson.put("StackFileContent", stackFile);
        updateJson.put("Env", env);
        updateJson.put("Prune", true);
        return updateJson;
    }
}
