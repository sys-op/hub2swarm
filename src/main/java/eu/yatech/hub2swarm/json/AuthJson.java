package eu.yatech.hub2swarm.json;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;

@RequiredArgsConstructor(staticName = "newAuth")
public @Data
class AuthJson {
    private final String username;
    private final String password;

    public JSONObject toJson() {
        JSONObject jo = new JSONObject();
        jo.put("username", username);
        jo.put("password", password);
        return jo;
    }
}
