package eu.yatech.hub2swarm.populators;

import eu.yatech.hub2swarm.service.UserService;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Slf4j
@Component
public class UserPopulator implements ApplicationListener<ContextRefreshedEvent> {

    private final UserService userService;
    private final Environment applicationProperties;

    private boolean alreadySetup = false;

    public UserPopulator(UserService userService, Environment props) {
        this.userService = userService;
        this.applicationProperties = props;
    }

    @Override
    @Transactional
    public void onApplicationEvent(@NonNull ContextRefreshedEvent event) {
        if(alreadySetup) return;
        createUserIfNotFound();
        alreadySetup = true;

    }

    @Transactional
        //method must be overridable
    void createUserIfNotFound() {
        if(userService.usersQty() > 0) return;

        Result result = userService.addUser(firstUserUsername(), firstUserPassword());
        if(! result.getMessage().equals(C.CREATED)) {
            log.error("Failed to create first user. Reason: " + result.getMessage());
        }
    }

    private String firstUserUsername() {
        String usernameForEnv = System.getenv(C.FIRST_USER_USERNAME_ENV);
        return StringUtils.isNotBlank(usernameForEnv) ? usernameForEnv : C.FIRST_USER_DEFAULT_USERNAME;
    }

    private String firstUserPassword() {
        String passwordFromEnv = System.getenv(C.FIRST_USER_USERNAME_PASSWORD);
        return StringUtils.isNotBlank(passwordFromEnv) ? passwordFromEnv : generateRandomPass();
    }

    private String generateRandomPass() {
        String password = RandomStringUtils.randomAlphanumeric(9);
        recordPasswordToFile(password);
        return password;
    }

    private void recordPasswordToFile(String password) {
        String fileName = applicationProperties.getProperty(C.GENERATED_PASSWORD_LOCATION_PROP);
        if(StringUtils.isBlank(fileName)) {
            log.error("Nowhere to save first user's password");
            System.exit(C.NOWHERE_TO_SAVE_ROOT_PASSWORD);
        }

        if(fileName.endsWith("/")) {
            fileName = fileName + "password";
        }

        File file = new File(fileName);
        if(! file.exists() && ! file.getParentFile().canWrite()) {
            log.error("Password file '" + fileName + "' doesn't exists and cannot be created");
            System.exit(C.NOWHERE_TO_SAVE_ROOT_PASSWORD);
        }

        if(! file.exists()) {
            try {
                boolean created = file.createNewFile();
                if(!created) {
                    log.error("Password file '" + fileName + "' cannot be created");
                    System.exit(C.NOWHERE_TO_SAVE_ROOT_PASSWORD);
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(C.NOWHERE_TO_SAVE_ROOT_PASSWORD);
            }
        }

        if(! file.canWrite()) {
            log.error("Password file '" + fileName + "' exists, but not writable");
            System.exit(C.NOWHERE_TO_SAVE_ROOT_PASSWORD);
        }

        try {
            FileUtils.write(file, password, StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(C.NOWHERE_TO_SAVE_ROOT_PASSWORD);
        }
    }
}
