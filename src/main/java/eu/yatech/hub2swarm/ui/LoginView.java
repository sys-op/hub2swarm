package eu.yatech.hub2swarm.ui;

import com.vaadin.flow.component.login.AbstractLogin;
import com.vaadin.flow.component.login.LoginI18n;
import com.vaadin.flow.component.login.LoginOverlay;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.VaadinService;
import com.vaadin.flow.server.VaadinServletRequest;
import com.vaadin.flow.server.VaadinServletResponse;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import eu.yatech.hub2swarm.service.UserService;
import eu.yatech.hub2swarm.utils.C;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;


@Slf4j
@SpringComponent
@UIScope
@Route(value = "login")
@Theme(value = Lumo.class, variant = Lumo.DARK)
public class LoginView extends LoginOverlay implements AfterNavigationObserver {
    private final AuthenticationProvider authenticationProvider;
    private final SessionAuthenticationStrategy sessionAuthenticationStrategy;
    private final UserService userService;
    private final Environment applicationProperties;

    private LoginI18n i18n;

    public LoginView(AuthenticationProvider authProvider, SessionAuthenticationStrategy sessionAuthenticationStrategy,
                     UserService userService, Environment env) {
        this.authenticationProvider = authProvider;
        this.sessionAuthenticationStrategy = sessionAuthenticationStrategy;
        this.userService = userService;
        this.applicationProperties = env;

        init();
    }

    private void init() {
        this.setAction("login");
        this.setOpened(true);
        this.setTitle("Hub2Swarm");
        this.setDescription("Login to Hub2Swarm");

        this.i18n = LoginI18n.createDefault();
        showInfoAboutFirstUserPasswordIfNeeded();
        catchLoginError();
        this.setI18n(i18n);

        this.addLoginListener(this::doLogin);
        this.setForgotPasswordButtonVisible(false);
    }

    private void doLogin(AbstractLogin.LoginEvent event) {
        final Authentication auth = new UsernamePasswordAuthenticationToken(event.getUsername(),
                event.getPassword());

        try {
            final Authentication authenticated = authenticationProvider.authenticate(auth);
            SecurityContextHolder.getContext().setAuthentication(authenticated);
            sessionAuthenticationStrategy.onAuthentication(auth,
                    ((VaadinServletRequest) VaadinService.getCurrentRequest()).getHttpServletRequest(),
                    ((VaadinServletResponse) VaadinService.getCurrentResponse()).getHttpServletResponse());
        } catch (Exception ex) {
            log.error(String.format("Login failed. Username: %s, Error: %s",
                    event.getUsername(), ex.getMessage()));
        }
    }

    private void catchLoginError() {
        Object ex = VaadinService.getCurrentRequest().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        if(ex instanceof Exception) {
            this.i18n.getErrorMessage().setTitle("Oops! Login Error occurred");
            this.i18n.getErrorMessage().setMessage(((Exception) ex).getMessage());
        }
    }

    @Override
    public void afterNavigation(AfterNavigationEvent event) {
        this.setError(event.getLocation().getQueryParameters().getParameters().containsKey("error"));
    }

    private void showInfoAboutFirstUserPasswordIfNeeded() {
        boolean isOnlyFirstUserInSystem = userService.lastUser() &&
                userService.getUserWithUsername(firstUserUsername()) != null;
        boolean firstUserPasswordStoredToFile = StringUtils.isBlank(System.getenv(C.FIRST_USER_USERNAME_PASSWORD));

        if (isOnlyFirstUserInSystem && firstUserPasswordStoredToFile) {
            String passwordFilePath = applicationProperties.getProperty(C.GENERATED_PASSWORD_LOCATION_PROP);
            String message = String.format("First time here? Login as '%s' with password from file %s",
                    firstUserUsername(), passwordFilePath);
            this.i18n.setAdditionalInformation(message);
        }
    }

    private String firstUserUsername() {
        String usernameForEnv = System.getenv(C.FIRST_USER_USERNAME_ENV);
        return StringUtils.isNotBlank(usernameForEnv) ? usernameForEnv : C.FIRST_USER_DEFAULT_USERNAME;
    }
}
