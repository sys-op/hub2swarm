package eu.yatech.hub2swarm.ui.element;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.Html;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class Error extends HorizontalLayout {
    @SuppressWarnings("FieldCanBeLocal")
    private final Icon warningIcon = new Icon(VaadinIcon.WARNING);

    public static Error withHtml(Html html) {
        return new Error(html);
    }

    public static Error withText(String text) {
        return new Error(new Html("<span>"+text+"</span>"));
    }

    private Error(Component message) {
        add(warningIcon, message);
        styleMe();
    }

    private void styleMe() {
        this.getStyle().set("background-color", "#ff5f57");
        this.getStyle().set("padding", ".75rem 1.25rem");
        this.getStyle().set("border", "1px solid transparent");
        this.getStyle().set("border-radius", ".25rem");
    }

}
