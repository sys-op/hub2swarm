package eu.yatech.hub2swarm.ui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import eu.yatech.hub2swarm.model.Swarm;
import eu.yatech.hub2swarm.service.SwarmService;
import eu.yatech.hub2swarm.utils.*;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;


@SpringComponent
@UIScope
@Route(value = "swarms", layout = RouterView.class)
public class SwarmView extends VerticalLayout {
    private Registration broadcasterRegistration;
    private final SwarmService swarmService;
    private final UrlValidator urlValidator = new UrlValidator("");

    private final VerticalLayout swarmSection = new VerticalLayout();

    private final H2 title = new H2("Swarms");

    private final VerticalLayout dataArea = new VerticalLayout();
    private final Text noItemsText = new Text("Here will be table of Swarms");
    private final Grid<Swarm> swarmGrid = new Grid<>(Swarm.class);

    private final VerticalLayout form = new VerticalLayout();
    private final TextField labelField = new TextField("Label", "MySwarm");
    private final TextField urlField = new TextField("URL", "https://my.domain.tld");

    private final HorizontalLayout buttons = new HorizontalLayout();
    private final Button addBtn = new Button("Add", this::addNewOrEdit);
    private final Button deleteBtn = new Button("Delete", this::delete);

    private final Text errorField = new Text("");

    public SwarmView(SwarmService swarmService) {
        this.swarmService = swarmService;
        init();
        setLayout();
        gridInit();
    }

    private void init() {
        addBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        deleteBtn.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteBtn.setEnabled(false); //disable by default
    }

    private void setLayout() {
        buttons.add(addBtn, deleteBtn);
        form.add(labelField, urlField, buttons);

        swarmSection.add(title);
        swarmSection.add(dataArea);
        swarmSection.add(form);
        swarmSection.add(errorField);

        //adding all to page
        add(swarmSection);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = Broadcaster.register(message -> ui.access(() -> {
            cleanFormValues();
            gridInit();

            if(StringUtils.isNotBlank(message)) {
                Notifier.doNotify(message).open();
            }
        }));
        //page init
        Broadcaster.broadcast("");
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        // Cleanup
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }

    private void gridInit() {
        dataArea.removeAll();
        if (swarmService.swarmsQty() == 0) {
            dataArea.add(noItemsText);
        } else {
            dataArea.add(swarmGrid);
            swarmGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
            swarmGrid.addSelectionListener(this::onSelect);
            swarmGrid.setItems(swarmService.getAllSwarms());
        }
    }

    private void onSelect(SelectionEvent<Grid<Swarm>, Swarm> gridSwarmSelectionEvent) {
        Optional<Swarm> selection = gridSwarmSelectionEvent.getFirstSelectedItem();
        selection.ifPresent(this::setFormValues);
        selection.ifPresent(this::toggleDeleteBtn);
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void addNewOrEdit(ClickEvent<Button> buttonClickEvent) {
        String label = labelField.getValue().trim();
        if(StringUtils.isAllEmpty(label)) {
            errorField.setText("Type correct Label");
            return;
        }
        String url = urlField.getValue().trim();
        if(StringUtils.isAllEmpty(url)) {
            errorField.setText("Type correct URL");
            return;
        }
        if(! urlValidator.isValid(url)) {
            errorField.setText("Type valid URL");
            return;
        }

        Result saveResult =  swarmService.addOrEditSwarm(label, url);
        if(saveResult.getMessage().equals(C.CREATED)) {
            Swarm swarm = (Swarm) saveResult.getPayload();
            String message = String.format("Swarm '%s' (%s) created", swarm.getLabel(),swarm.getUrl());
            Broadcaster.broadcast(message);
        } else if(saveResult.getMessage().equals(C.UPDATED)) {
            Swarm updatedSwarm = (Swarm) saveResult.getPayload();
            String message = String.format("Swarm %d (%s) was updated", updatedSwarm.getId(),
                    updatedSwarm.getLabel());
            Broadcaster.broadcast(message);
        } else {
            errorField.setText(saveResult.getMessage());
        }
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void delete(ClickEvent<Button> buttonClickEvent) {
        String label = labelField.getValue();
        if(label == null) {
            errorField.setText("Nothing selected");
            return;
        }
        Swarm swarm = swarmService.getSwarmByLabel(label);
        String deleteResult = swarmService.deleteSwarm(swarm);
        if(deleteResult.equals(C.SUCCESS)) {
            String message = String.format("Swarm '%s' (%s) deleted", swarm.getLabel(), swarm.getUrl());
            Broadcaster.broadcast(message);
        } else {
            errorField.setText(deleteResult);
        }
    }

    private void cleanFormValues() {
        labelField.clear();
        urlField.clear();
        errorField.setText("");
        deleteBtn.setEnabled(false);
    }

    private void setFormValues(Swarm selectedItem) {
        cleanFormValues();
        labelField.setValue(selectedItem.getLabel());
        urlField.setValue(selectedItem.getUrl());
    }

    private void toggleDeleteBtn(Swarm selectedItem) {
        if(swarmService.canDelete(selectedItem)) {
            deleteBtn.setEnabled(true);
        } else {
            deleteBtn.setEnabled(false);
        }
    }

}
