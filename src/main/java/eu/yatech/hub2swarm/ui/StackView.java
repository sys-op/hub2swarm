package eu.yatech.hub2swarm.ui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.TextRenderer;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import eu.yatech.hub2swarm.app.ImageReplacePolicy;
import eu.yatech.hub2swarm.model.DockerImage;
import eu.yatech.hub2swarm.model.Stack;
import eu.yatech.hub2swarm.model.Swarm;
import eu.yatech.hub2swarm.service.ImageService;
import eu.yatech.hub2swarm.service.StackService;
import eu.yatech.hub2swarm.service.SwarmService;
import eu.yatech.hub2swarm.ui.element.Error;
import eu.yatech.hub2swarm.utils.Broadcaster;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Notifier;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Optional;


@SpringComponent
@UIScope
@Route(value = "stacks", layout = RouterView.class)
public class StackView extends VerticalLayout {
    private Registration broadcasterRegistration;
    private final StackService stackService;
    private final SwarmService swarmService;
    private final ImageService imageService;

    private final Binder<Stack> binder = new Binder<>();

    private final VerticalLayout stackSection = new VerticalLayout();

    private final H2 title = new H2("Stacks");

    private final VerticalLayout dataArea = new VerticalLayout();
    private final Text noItemsText = new Text("Here will be table of Stacks");
    private final Grid<Stack> stackGrid = new Grid<>(Stack.class);

    private final VerticalLayout form = new VerticalLayout();
    private final HorizontalLayout stackFields = new HorizontalLayout();
    private final TextField stackNameField = new TextField("Stack Name", "myStack");
    private final NumberField stackIdField = new NumberField("Stack ID in Swarm");
    private final Select<Swarm> swarmSelect = new Select<>();
    private final Select<DockerImage> imageSelect = new Select<>();
    private final RadioButtonGroup<ImageReplacePolicy> replacePolicyRadio = new RadioButtonGroup<>();

    private final HorizontalLayout buttons = new HorizontalLayout();
    private final Button addBtn = new Button("Add", this::addNewOrEdit);
    private final Button deleteBtn = new Button("Delete", this::delete);

    private final VerticalLayout errorZone = new VerticalLayout();

    public StackView(StackService stackService, SwarmService swarmService, ImageService imageService) {
        this.stackService = stackService;
        this.swarmService = swarmService;
        this.imageService = imageService;
        init();
        setLayout();
        gridInit();
        swarmSelectInit();
        imageSelectInit();
    }

    private void init() {
        stackGrid.removeAllColumns();
        stackGrid.addColumn(Stack::getStackName).setHeader("Stack Name");
        stackGrid.addColumn(Stack::getStackId).setHeader("Stack ID");
        stackGrid.addColumn(Stack::getSwarmLabel).setHeader("Swarm");
        stackGrid.addColumn(Stack::getImageLabel).setHeader("Docker Image");

        stackIdField.setMin(1);
        stackIdField.setStep(1);
        stackIdField.setValue(1d);
        stackIdField.setHasControls(true);

        swarmSelect.setLabel("Swarm");
        swarmSelect.setEmptySelectionAllowed(false);
        swarmSelect.setEmptySelectionCaption("Select Swarm");
        swarmSelect.setItemLabelGenerator(Swarm::getLabel);
        swarmSelect.setTextRenderer(Swarm::getLabel);
        binder.forField(swarmSelect).bind(Stack::getSwarm, Stack::setSwarm);

        imageSelect.setLabel("Docker Image");
        imageSelect.setEmptySelectionAllowed(false);
        imageSelect.setEmptySelectionCaption("Select Docker Image");
        imageSelect.setItemLabelGenerator(DockerImage::toDockerImage);
        imageSelect.setTextRenderer(DockerImage::toDockerImage);
        binder.forField(imageSelect).bind(Stack::getImage, Stack::setImage);

        replacePolicyRadio.setLabel("Replace image in stack file while running stack update?");
        replacePolicyRadio.setItems(ImageReplacePolicy.NO_REPLACE, ImageReplacePolicy.REPLACE);
        replacePolicyRadio.setRenderer(new TextRenderer<>(ImageReplacePolicy::name));

        addBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        deleteBtn.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteBtn.setEnabled(false); //disable by default

    }
    private void setLayout() {
        buttons.add(addBtn, deleteBtn);
        stackFields.add(stackNameField, stackIdField);
        form.add(stackFields, swarmSelect, imageSelect, replacePolicyRadio, buttons);

        stackSection.add(title);
        stackSection.add(dataArea);
        stackSection.add(form);
        stackSection.add(errorZone);

        //adding all to page
        add(stackSection);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = Broadcaster.register(message -> ui.access(() -> {
            cleanFormValues();
            gridInit();
            swarmSelectInit();
            imageSelectInit();

            if(StringUtils.isNotBlank(message)) {
                Notifier.doNotify(message).open();
            }
        }));
        //page init
        Broadcaster.broadcast("");
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        // Cleanup
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }

    private void gridInit() {
        dataArea.removeAll();
        if (stackService.stackQty() == 0) {
            dataArea.add(noItemsText);
        } else {
            dataArea.add(stackGrid);
            stackGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
            stackGrid.addSelectionListener(this::onSelect);

            stackGrid.setItems(stackService.getAllStacks());
        }
    }

    private void swarmSelectInit() {
        List<Swarm> allSwarms = swarmService.getAllSwarms();
        if(allSwarms.isEmpty()) {
            swarmSelect.setEnabled(false);
            String swarmUrl = RouteConfiguration.forApplicationScope().getUrl(SwarmView.class);
            errorZone.add(Error.withHtml(new Html("<span>There is no Swarms registered in system. " +
                    "Register one <a href='"+swarmUrl+"'>here</a></span>")));
        } else {
            swarmSelect.setItems(allSwarms);
        }
    }

    private void imageSelectInit() {
        List<DockerImage> allImages = imageService.getAllImages();
        if(allImages.isEmpty()) {
            imageSelect.setEnabled(false);
            String imageUrl = RouteConfiguration.forApplicationScope().getUrl(ImageView.class);
            errorZone.add(Error.withHtml(new Html("<span>There is no Docker images registered in system. " +
                    "Register one <a href='"+imageUrl+"'>here</a></span>")));
        } else {
            imageSelect.setItems(allImages);
        }
    }

    private void onSelect(SelectionEvent<Grid<Stack>, Stack> gridSwarmSelectionEvent) {
        Optional<Stack> selection = gridSwarmSelectionEvent.getFirstSelectedItem();
        selection.ifPresent(this::setFormValues);
        selection.ifPresent(this::toggleDeleteBtn);
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    //TODO logic check
    private void addNewOrEdit(ClickEvent<Button> buttonClickEvent) {
        String stackName = stackNameField.getValue().trim();
        int stackId = stackIdField.getValue().intValue();
        Swarm selectedSwarm = swarmSelect.getValue();
        DockerImage selectedImage = imageSelect.getValue();
        ImageReplacePolicy replacePolicy = replacePolicyRadio.getValue();

        if(StringUtils.isAllEmpty(stackName)) {
            errorZone.add(Error.withText("Type correct Stack Name"));
            return;
        }

        if(stackId <= 0) {
            errorZone.add(Error.withText("Stack ID is always positive number. " +
                    "You can find your stack ID in address bar, when you open stack in portainer"));
            return;
        }

        if(selectedSwarm == null) {
            errorZone.add(Error.withText(("no Swarm selected")));
            return;
        }

        if(selectedImage == null) {
            errorZone.add(Error.withText(("no Docker image selected")));
            return;
        }

        if(replacePolicy == null) {
            replacePolicy = ImageReplacePolicy.NO_REPLACE;
        }

        Result saveResult =  stackService.addOrEditStack(stackName, stackId, selectedSwarm, selectedImage, replacePolicy);
        if(saveResult.getMessage().equals(C.CREATED)) {
            Stack service = (Stack) saveResult.getPayload();
            String message = String.format("Stack '%s' created", service.getStackName());
            Broadcaster.broadcast(message);
        } else if(saveResult.getMessage().equals(C.UPDATED)) {
            Stack updatedService = (Stack) saveResult.getPayload();
            String message = String.format("Stack %s@%s was updated", updatedService.getStackName(),
                    updatedService.getSwarm().getLabel());
            Broadcaster.broadcast(message);
        } else {
            errorZone.add(Error.withText((saveResult.getMessage())));
        }
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void delete(ClickEvent<Button> buttonClickEvent) {
        String stackName = stackNameField.getValue();
        Swarm swarm = swarmSelect.getValue();
        DockerImage image = imageSelect.getValue();

        if(StringUtils.isBlank(stackName)) {
            errorZone.add(Error.withText("Nothing selected"));
            return;
        }
        if(swarm == null) {
            errorZone.add(Error.withText("Nothing selected"));
            return;
        }

        Stack stack = stackService.getStackByNameAndSwarm(stackName, swarm, image);
        String deleteResult = stackService.deleteStack(stack);
        if(deleteResult.equals(C.SUCCESS)) {
            Broadcaster.broadcast("");
            String message = String.format("Stack '%s' deleted", stack.getStackName());
            Notifier.doNotify(message).open();
        } else {
            errorZone.add(Error.withText(deleteResult));
        }
    }

    private void cleanFormValues() {
        stackNameField.clear();
        stackIdField.clear();
        swarmSelect.clear();
        imageSelect.clear();
        replacePolicyRadio.clear();
        errorZone.removeAll();
        deleteBtn.setEnabled(false);
    }

    private void setFormValues(Stack selectedItem) {
        cleanFormValues();
        stackNameField.setValue(selectedItem.getStackName());
        stackIdField.setValue((double) selectedItem.getStackId());
        swarmSelect.setValue(selectedItem.getSwarm());
        imageSelect.setValue(selectedItem.getImage());
        replacePolicyRadio.setValue(selectedItem.getReplacePolicy());
    }

    private void toggleDeleteBtn(Stack selectedItem) {
        if(stackService.canDelete(selectedItem)) {
            deleteBtn.setEnabled(true);
        } else {
            deleteBtn.setEnabled(false);
        }
    }
}
