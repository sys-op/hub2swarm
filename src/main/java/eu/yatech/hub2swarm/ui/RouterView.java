package eu.yatech.hub2swarm.ui;

import com.github.appreciated.app.layout.behaviour.Behaviour;
import com.github.appreciated.app.layout.builder.AppLayoutBuilder;
import com.github.appreciated.app.layout.component.appbar.AppBarBuilder;
import com.github.appreciated.app.layout.component.menu.left.builder.LeftAppMenuBuilder;
import com.github.appreciated.app.layout.component.menu.left.items.LeftNavigationItem;
import com.github.appreciated.app.layout.router.AppLayoutRouterLayout;
import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.DetachEvent;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.server.*;
import com.vaadin.flow.shared.communication.PushMode;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.theme.Theme;
import com.vaadin.flow.theme.lumo.Lumo;
import eu.yatech.hub2swarm.config.SecurityConfig;
import eu.yatech.hub2swarm.config.security.SecurityUtils;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;


@SpringComponent
@UIScope
@Push
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@PWA(name = "Hub2Swarm", shortName = "hub2swarm", iconPath="img/icons/icon.png",
        offlinePath = "offline-page.html",
        offlineResources = {"img/icons/icon.png"},
        description = "Application with Docker Hub webhook and deployer to Portainer")
@Theme(value = Lumo.class)
public class RouterView extends AppLayoutRouterLayout implements PageConfigurator {

    public RouterView() {
        if (SecurityUtils.isUserLoggedIn()) {
            Button logoutButton = new Button("", this::onLogout);
            logoutButton.setIcon(VaadinIcon.SIGN_OUT.create());

            init(AppLayoutBuilder
                    .get(Behaviour.LEFT_RESPONSIVE_HYBRID)
                    .withTitle("Hub2Swarm")
                    .withAppBar(AppBarBuilder.get()
                            .add(logoutButton)
                            .build())
                    .withAppMenu(LeftAppMenuBuilder.get()
                            .add(new LeftNavigationItem("Images", VaadinIcon.COPY.create(), ImageView.class))
                            .add(new LeftNavigationItem("Stacks", VaadinIcon.COMPILE.create(), StackView.class))
                            .add(new LeftNavigationItem("Deployments", VaadinIcon.DOWNLOAD.create(), DeploymentView.class))
                            .add(new LeftNavigationItem("Swarms", VaadinIcon.CUBES.create(), SwarmView.class))
                            .add(new LeftNavigationItem("Creds", VaadinIcon.SAFE.create(), CredView.class))
                            .add(new LeftNavigationItem("Users", VaadinIcon.USERS.create(), UserView.class))
                            .build())
                    .build());
        } else {
            getUI().ifPresent(ui -> ui.navigate(LoginView.class));
        }
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void onLogout(ClickEvent<Button> buttonClickEvent) {
        VaadinService.getCurrentRequest().getWrappedSession().invalidate();
        new SecurityContextLogoutHandler()
                .logout(((VaadinServletRequest) VaadinService.getCurrentRequest())
                        .getHttpServletRequest(), null, null);

        UI ui = UI.getCurrent();
        ui.getPage().executeJavaScript("window.location.href='" + SecurityConfig.LOGOUT_SUCCESS_URL + "'");
        ui.getPushConfiguration().setPushMode(PushMode.DISABLED);
        ui.getSession().close();

    }

    /**
     * Using the @Theme Annotation to set the Dark Theme causes issues with shadows which will appear in
     * the wrong color making them seemingly invisible. Instead do it the following way as long as the issue is not
     * solved (https://github.com/vaadin/flow/issues/4765)
     */
    @Override
    protected void onAttach(AttachEvent attachEvent) {
        super.onAttach(attachEvent);
        getUI().ifPresent(ui -> ui.getPage()
                .executeJavaScript("document.documentElement.setAttribute(\"theme\",\"dark\")"));
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        super.onDetach(detachEvent);
    }

    @Override
    public void configurePage(InitialPageSettings settings) {
        settings.addFavIcon("icon", "/img/icons/icon.png", "512x512");
    }
}
