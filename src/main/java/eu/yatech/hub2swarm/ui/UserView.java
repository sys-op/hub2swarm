package eu.yatech.hub2swarm.ui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import eu.yatech.hub2swarm.model.User;
import eu.yatech.hub2swarm.service.UserService;
import eu.yatech.hub2swarm.ui.element.Error;
import eu.yatech.hub2swarm.utils.Broadcaster;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Notifier;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;


@SpringComponent
@UIScope
@Route(value = "users", layout = RouterView.class)
public class UserView extends VerticalLayout {
    private Registration broadcasterRegistration;
    private final UserService userService;

    private final VerticalLayout userSection = new VerticalLayout();

    private final H2 title = new H2("Users");

    private final VerticalLayout dataArea = new VerticalLayout();
    private final Text noItemsText = new Text("Here will be table of Users. You should not see it...");
    private final Grid<User> userGrid = new Grid<>(User.class);

    private final VerticalLayout form = new VerticalLayout();
    private final TextField usernameField = new TextField("Username", "user");
    private final PasswordField passwordField = new PasswordField("Password", "plainTextPassword");
    private final RadioButtonGroup<Boolean> userEnabledRadio = new RadioButtonGroup<>();
    private final RadioButtonGroup<Boolean> userLockedRadio = new RadioButtonGroup<>();

    private final HorizontalLayout buttons = new HorizontalLayout();
    private final Button addBtn = new Button("Add", this::addNewOrEdit);
    private final Button deleteBtn = new Button("Delete", this::delete);

    private final VerticalLayout errorZone = new VerticalLayout();


    public UserView(UserService userService) {
        this.userService = userService;

        init();
        setLayout();
        gridInit();
    }

    private void init() {
        userGrid.removeAllColumns();
        userGrid.addColumn(User::getUsername).setHeader("Username");
        userGrid.addColumn(User::getPassword).setHeader("Encrypted Password");
        userGrid.addColumn(User::isEnabled).setHeader("Enabled");
        userGrid.addColumn(User::isLocked).setHeader("Locked");

        userEnabledRadio.setLabel("User enabled");
        userEnabledRadio.setItems(Boolean.TRUE, Boolean.FALSE);

        userLockedRadio.setLabel("User locked");
        userLockedRadio.setItems(Boolean.TRUE, Boolean.FALSE);

        addBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        deleteBtn.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteBtn.setEnabled(false); //disable by default
    }

    private void setLayout() {
        buttons.add(addBtn, deleteBtn);

        form.add(usernameField, passwordField, userEnabledRadio, userLockedRadio, buttons);

        userSection.add(title);
        userSection.add(dataArea);
        userSection.add(form);
        userSection.add(errorZone);

        //adding all to page
        add(userSection);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = Broadcaster.register(message -> ui.access(() -> {
            cleanFormValues();
            gridInit();
            if(StringUtils.isNotBlank(message)) {
                Notifier.doNotify(message).open();
            }
        }));
        //page init
        Broadcaster.broadcast("");
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        // Cleanup
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }

    private void gridInit() {
        dataArea.removeAll();
        if (userService.usersQty() == 0) {
            dataArea.add(noItemsText);
        } else {
            dataArea.add(userGrid);
            userGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
            userGrid.addSelectionListener(this::onSelect);

            userGrid.setItems(userService.getAllUsers());
        }
    }

    private void onSelect(SelectionEvent<Grid<User>, User> gridSwarmSelectionEvent) {
        Optional<User> selection = gridSwarmSelectionEvent.getFirstSelectedItem();
        selection.ifPresent(this::setFormValues);
        selection.ifPresent(this::toggleDeleteBtn);
        selection.ifPresent(this::toggleRadioButtons);
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void addNewOrEdit(ClickEvent<Button> buttonClickEvent) {
        String username = usernameField.getValue().trim();
        String password = passwordField.getValue().trim();

        if(StringUtils.isAllEmpty(username)) {
            errorZone.add(Error.withText("Type correct Username"));
            return;
        }

        if(StringUtils.isAllEmpty(password)) {
            errorZone.add(Error.withText("Type correct Password"));
            return;
        }


        Result saveResult =  userService.addOrEditUser(username, password, true, true);
        if(saveResult.getMessage().equals(C.CREATED)) {
            User user = (User) saveResult.getPayload();
            String message = "User created";
            Broadcaster.broadcast(message);
        } else if(saveResult.getMessage().equals(C.UPDATED)) {
            User updatedUser = (User) saveResult.getPayload();
            String message = String.format("User %s was updated", updatedUser.getUsername());
            Broadcaster.broadcast(message);
        } else {
            errorZone.add(Error.withText((saveResult.getMessage())));
        }
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void delete(ClickEvent<Button> buttonClickEvent) {
        String username = usernameField.getValue();

        if(StringUtils.isBlank(username)) {
            errorZone.add(Error.withText("Nothing selected"));
            return;
        }

        User user = userService.getUserWithUsername(username);
        String deleteResult = userService.deleteUser(user);
        if(deleteResult.equals(C.SUCCESS)) {
            String message = String.format("Stack '%s' deleted", user.getUsername());
            Broadcaster.broadcast(message);
        } else {
            errorZone.add(Error.withText(deleteResult));
        }
    }

    private void cleanFormValues() {
        usernameField.clear();
        passwordField.clear();
        errorZone.removeAll();
        deleteBtn.setEnabled(false);
    }

    private void setFormValues(User selectedItem) {
        cleanFormValues();
        usernameField.setValue(selectedItem.getUsername());
        passwordField.setValue(selectedItem.getPassword());
        userEnabledRadio.setValue(selectedItem.isEnabled());
        userLockedRadio.setValue(selectedItem.isLocked());
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void toggleRadioButtons(User selectedItem) {
        if(userService.lastUser()) {
            userEnabledRadio.setEnabled(false);
            userLockedRadio.setEnabled(false);
        }
        if(userService.lastEnabledUser()) {
            userEnabledRadio.setEnabled(false);
        }
        if(userService.lastUnlockedUser()) {
            userLockedRadio.setEnabled(false);
        }
    }

    private void toggleDeleteBtn(User selectedItem) {
        if(userService.canDelete(selectedItem)) {
            deleteBtn.setEnabled(true);
        } else {
            deleteBtn.setEnabled(false);
        }
    }
}
