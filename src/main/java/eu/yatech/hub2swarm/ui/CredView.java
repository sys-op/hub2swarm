package eu.yatech.hub2swarm.ui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import eu.yatech.hub2swarm.model.Cred;
import eu.yatech.hub2swarm.model.Swarm;
import eu.yatech.hub2swarm.service.CredService;
import eu.yatech.hub2swarm.service.SwarmService;
import eu.yatech.hub2swarm.ui.element.Error;
import eu.yatech.hub2swarm.utils.Broadcaster;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Notifier;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.encrypt.TextEncryptor;

import java.util.List;
import java.util.Optional;


@SpringComponent
@UIScope
@Route(value = "creds", layout = RouterView.class)
public class CredView extends VerticalLayout {
    private Registration broadcasterRegistration;
    private final CredService credService;
    private final SwarmService swarmService;
    private final TextEncryptor encryptor;

    private final Binder<Cred> binder = new Binder<>();
    private boolean isPasswordInFormPlain = true;

    private final VerticalLayout credSection = new VerticalLayout();

    private final H2 title = new H2("Credentials");

    private final VerticalLayout dataArea = new VerticalLayout();
    private final Text noItemsText = new Text("Here will be table of Credentials");
    private final Grid<Cred> credGrid = new Grid<>(Cred.class);

    private final VerticalLayout form = new VerticalLayout();
    private final TextField usernameField = new TextField("Username", "user");
    private final HorizontalLayout passwordFields = new HorizontalLayout();
    private final PasswordField passwordField = new PasswordField("", "plainTextPassword");
    private final Button decryptButton = new Button("Decrypt", this::decrypt);

    private final Select<Swarm> swarmSelect = new Select<>();

    private final HorizontalLayout buttons = new HorizontalLayout();
    private final Button addBtn = new Button("Add", this::addNewOrEdit);
    private final Button deleteBtn = new Button("Delete", this::delete);

    private final VerticalLayout errorZone = new VerticalLayout();


    public CredView(CredService credService, SwarmService swarmService, TextEncryptor encryptor) {
        this.credService = credService;
        this.swarmService = swarmService;
        this.encryptor = encryptor;

        init();
        setLayout();
        gridInit();
        swarmSelectInit();
    }

    private void init() {
        credGrid.removeAllColumns();
        credGrid.addColumn(Cred::getUsername).setHeader("Username");
        credGrid.addColumn(Cred::getPassword).setHeader("Encrypted Password");
        credGrid.addColumn(Cred::getSwarmLabel).setHeader("Swarm");

        passwordField.addValueChangeListener(this::onPasswordInput);
        decryptButton.setEnabled(! isPasswordInFormPlain);

        swarmSelect.setLabel("Swarm");
        swarmSelect.setEmptySelectionAllowed(false);
        swarmSelect.setEmptySelectionCaption("Select Swarm");
        swarmSelect.setItemLabelGenerator(Swarm::getLabel);
        swarmSelect.setTextRenderer(Swarm::getLabel);
        binder.forField(swarmSelect).bind(Cred::getSwarm, Cred::setSwarm);

        addBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        deleteBtn.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteBtn.setEnabled(false); //disable by default

    }

    private void setLayout() {
        buttons.add(addBtn, deleteBtn);

        passwordFields.add(passwordField, decryptButton);
        form.add(usernameField, passwordFields, swarmSelect, buttons);

        credSection.add(title);
        credSection.add(dataArea);
        credSection.add(form);
        credSection.add(errorZone);

        //adding all to page
        add(credSection);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = Broadcaster.register(message -> ui.access(() -> {
            cleanFormValues();
            gridInit();
            swarmSelectInit();
            if(StringUtils.isNotBlank(message)) {
                Notifier.doNotify(message).open();
            }
        }));
        //page init
        Broadcaster.broadcast("");
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        // Cleanup
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }

    private void gridInit() {
        dataArea.removeAll();
        if (credService.credQty() == 0) {
            dataArea.add(noItemsText);
        } else {
            dataArea.add(credGrid);
            credGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
            credGrid.addSelectionListener(this::onSelect);

            credGrid.setItems(credService.getAllCreds());
        }
    }

    private void swarmSelectInit() {
        List<Swarm> allSwarms = swarmService.getAllSwarms();
        if(allSwarms.isEmpty()) {
            swarmSelect.setEnabled(false);
            String swarmUrl = RouteConfiguration.forApplicationScope().getUrl(SwarmView.class);
            errorZone.add(Error.withHtml(new Html("<span>There is no Swarms registered in system. " +
                    "Register one <a href='"+swarmUrl+"'>here</a></span>")));
        } else {
            swarmSelect.setItems(allSwarms);
        }
    }

    private void onSelect(SelectionEvent<Grid<Cred>, Cred> gridSwarmSelectionEvent) {
        Optional<Cred> selection = gridSwarmSelectionEvent.getFirstSelectedItem();
        selection.ifPresent(this::setFormValues);
        selection.ifPresent(this::toggleDeleteBtn);
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void addNewOrEdit(ClickEvent<Button> buttonClickEvent) {
        String username = usernameField.getValue().trim();
        String password = passwordField.getValue().trim();
        Swarm selectedSwarm = swarmSelect.getValue();

        if(StringUtils.isAllEmpty(username)) {
            errorZone.add(Error.withText("Type correct Username"));
            return;
        }

        if(StringUtils.isAllEmpty(password)) {
            errorZone.add(Error.withText("Type correct Password"));
            return;
        }

        if(selectedSwarm == null) {
            errorZone.add(Error.withText(("no Swarm selected")));
            return;
        }

        String encryptedPassword = isPasswordInFormPlain ? encryptor.encrypt(password) : password;

        Result saveResult =  credService.addOrEditCred(username, encryptedPassword, selectedSwarm);
        if(saveResult.getMessage().equals(C.CREATED)) {
            Cred cred = (Cred) saveResult.getPayload();
            String message = "Credentials created";
            Broadcaster.broadcast(message);
        } else if(saveResult.getMessage().equals(C.UPDATED)) {
            Cred updatedCreds = (Cred) saveResult.getPayload();
            String message = String.format("Credentials %d was updated", updatedCreds.getId());
            Broadcaster.broadcast(message);
        } else {
            errorZone.add(Error.withText((saveResult.getMessage())));
        }
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void delete(ClickEvent<Button> buttonClickEvent) {
        String username = usernameField.getValue();
        Swarm swarm = swarmSelect.getValue();

        if(StringUtils.isBlank(username)) {
            errorZone.add(Error.withText("Nothing selected"));
            return;
        }
        if(swarm == null) {
            errorZone.add(Error.withText("Nothing selected"));
            return;
        }

        Cred cred = credService.getCredByUsernameAndSwarm(username, swarm);
        String deleteResult = credService.deleteCred(cred);
        if(deleteResult.equals(C.SUCCESS)) {
            String message = String.format("Stack '%d' deleted", cred.getId());
            Broadcaster.broadcast(message);
        } else {
            errorZone.add(Error.withText(deleteResult));
        }
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void onPasswordInput(AbstractField.ComponentValueChangeEvent<PasswordField, String> passwordFieldStringComponentValueChangeEvent) {
        decryptButton.setEnabled(! isPasswordInFormPlain);
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void decrypt(ClickEvent<Button> buttonClickEvent) {
        String cryptedPass = passwordField.getValue();
        String plainPass;
        try {
            plainPass = encryptor.decrypt(cryptedPass);
        }catch (Exception e) {
            errorZone.add("Failed to decrypt. Maybe password is already in plain text or damaged");
            plainPass = cryptedPass;
        }

        isPasswordInFormPlain = true; passwordField.setValue(plainPass);
    }

    private void cleanFormValues() {
        usernameField.clear();
        isPasswordInFormPlain = true; passwordField.clear();
        swarmSelect.clear();
        errorZone.removeAll();
        deleteBtn.setEnabled(false);
    }

    private void setFormValues(Cred selectedItem) {
        cleanFormValues();
        usernameField.setValue(selectedItem.getUsername());

        isPasswordInFormPlain = false; passwordField.setValue(selectedItem.getPassword());

        swarmSelect.setValue(selectedItem.getSwarm());
    }

    private void toggleDeleteBtn(Cred selectedItem) {
        if(credService.canDelete(selectedItem)) {
            deleteBtn.setEnabled(true);
        } else {
            deleteBtn.setEnabled(false);
        }
    }
}
