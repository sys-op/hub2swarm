package eu.yatech.hub2swarm.ui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import eu.yatech.hub2swarm.model.DockerImage;
import eu.yatech.hub2swarm.service.ImageService;
import eu.yatech.hub2swarm.utils.Broadcaster;
import eu.yatech.hub2swarm.utils.C;
import eu.yatech.hub2swarm.utils.Notifier;
import eu.yatech.hub2swarm.utils.Result;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;


@SpringComponent
@UIScope
@Route(value = "", layout = RouterView.class)
public class ImageView extends VerticalLayout {
    private Registration broadcasterRegistration;
    private final ImageService imageService;

    private final VerticalLayout imageSection = new VerticalLayout();

    private final H2 title = new H2("Images");

    private final VerticalLayout dataArea = new VerticalLayout();
    private final Text noItemsText = new Text("Here will be table of Docker Images");
    private final Grid<DockerImage> imageGrid = new Grid<>(DockerImage.class);

    private final VerticalLayout form = new VerticalLayout();

    private final HorizontalLayout fields = new HorizontalLayout();
    private final TextField repoField = new TextField("Repo", "repo/my_image");
    private final TextField tagField = new TextField("Tag", "tag");

    private final HorizontalLayout buttons = new HorizontalLayout();
    private final Button addBtn = new Button("Add", this::addNewOrEdit);
    private final Button deleteBtn = new Button("Delete", this::delete);

    private final Text errorField = new Text("");

    public ImageView(ImageService imageService) {
        this.imageService = imageService;
        init();
        setLayout();
        gridInit();
    }

    private void init() {
        imageGrid.removeAllColumns();
        imageGrid.addColumn(new ComponentRenderer<>(imageService::mappingStatus)).setHeader("Status");
        imageGrid.addColumn(DockerImage::toDockerImage).setHeader("Image").setSortable(true);
        imageGrid.addColumn(new ComponentRenderer<>(imageService::listStacksByImage)).setHeader("Stacks");

        addBtn.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        deleteBtn.addThemeVariants(ButtonVariant.LUMO_ERROR);
        deleteBtn.setEnabled(false); //disable by default
    }

    private void setLayout() {
        fields.add(repoField, tagField);
        buttons.add(addBtn, deleteBtn);

        form.add(fields, buttons);

        imageSection.add(title);
        imageSection.add(dataArea);
        imageSection.add(form);
        imageSection.add(errorField);

        //adding all to page
        add(imageSection);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = Broadcaster.register(message -> ui.access(() -> {
            cleanFormValues();
            gridInit();

            if(StringUtils.isNotBlank(message)) {
                Notifier.doNotify(message).open();
            }
        }));
        //page init
        Broadcaster.broadcast("");
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        // Cleanup
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }

    private void gridInit() {
        dataArea.removeAll();
        if (imageService.imagesQty() == 0) {
            dataArea.add(noItemsText);
        } else {
            dataArea.add(imageGrid);
            imageGrid.setSelectionMode(Grid.SelectionMode.SINGLE);
            imageGrid.addSelectionListener(this::onSelect);
            imageGrid.setItems(imageService.getAllImages());
        }
    }

    private void onSelect(SelectionEvent<Grid<DockerImage>, DockerImage> gridImageSelectionEvent) {
        Optional<DockerImage> selection = gridImageSelectionEvent.getFirstSelectedItem();
        selection.ifPresent(this::setFormValues);
        selection.ifPresent(this::toggleDeleteBtn);
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void addNewOrEdit(ClickEvent<Button> buttonClickEvent) {
        String repo = repoField.getValue().trim();
        if(StringUtils.isAllEmpty(repo) || (! repo.contains("/"))) {
            errorField.setText("Type correct Repo. Example: myrepo/image");
            return;
        }
        String tag = tagField.getValue().trim();
        if(StringUtils.isAllEmpty(tag)) {
            errorField.setText("Type correct tag");
            return;
        }

        Result saveResult =  imageService.addOrEditDockerImage(repo, tag);
        if(saveResult.getMessage().equals(C.CREATED)) {
            DockerImage image = (DockerImage) saveResult.getPayload();
            String message = String.format("Image '%s:%s' created", image.getRepo(),image.getTag());
            Broadcaster.broadcast(message);

        } else if(saveResult.getMessage().equals(C.UPDATED)) {
            DockerImage updatedImage = (DockerImage) saveResult.getPayload();
            String message = String.format("Image %d was updated", updatedImage.getId());
            Broadcaster.broadcast(message);
        } else {
            errorField.setText(saveResult.getMessage());
        }
    }

    @SuppressWarnings("unused") //Not needed, but in signature
    private void delete(ClickEvent<Button> buttonClickEvent) {
        String repo = repoField.getValue();
        String tag = tagField.getValue();

        if(StringUtils.isBlank(repo) || StringUtils.isBlank(tag)) {
            errorField.setText("Nothing selected");
            return;
        }

        DockerImage image = imageService.getImageByRepoAndTag(repo, tag);
        String deleteResult = imageService.deleteImage(image);
        if(deleteResult.equals(C.SUCCESS)) {
            String message = String.format("Image '%s:%s' deleted", image.getRepo(),image.getTag());
            Broadcaster.broadcast(message);
        } else {
            errorField.setText(deleteResult);
        }
    }

    private void cleanFormValues() {
        repoField.clear();
        tagField.clear();
        errorField.setText("");
        deleteBtn.setEnabled(false);
    }

    private void setFormValues(DockerImage selectedItem) {
        cleanFormValues();
        repoField.setValue(selectedItem.getRepo());
        tagField.setValue(selectedItem.getTag());
    }

    private void toggleDeleteBtn(DockerImage selectedItem) {
        if(imageService.canDelete(selectedItem)) {
            deleteBtn.setEnabled(true);
        } else {
            deleteBtn.setEnabled(false);
        }
    }

}
