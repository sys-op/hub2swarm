package eu.yatech.hub2swarm.ui;

import com.vaadin.flow.component.*;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H2;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NativeButtonRenderer;
import com.vaadin.flow.data.renderer.TemplateRenderer;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.shared.Registration;
import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import eu.yatech.hub2swarm.app.DeploymentStatus;
import eu.yatech.hub2swarm.app.Updater;
import eu.yatech.hub2swarm.model.Deployment;
import eu.yatech.hub2swarm.service.DeploymentService;
import eu.yatech.hub2swarm.utils.Broadcaster;
import eu.yatech.hub2swarm.utils.Notifier;
import org.apache.commons.lang3.StringUtils;

import java.util.Optional;

@SpringComponent
@UIScope
@Route(value = "deployments", layout = RouterView.class)
public class DeploymentView extends VerticalLayout {
    private Registration broadcasterRegistration;
    private final DeploymentService deploymentService;
    private final Updater updater;

    private final H2 title = new H2("Deployments");

    private final VerticalLayout dataArea = new VerticalLayout();

    private final ComboBox<DeploymentStatus> deploymentStatusSelect = new ComboBox<>("Filter by Deployment Status:");
    private final Text noItemsText = new Text("Here will be table of Deployments");
    private final Grid<Deployment> grid = new Grid<>(Deployment.class);

    private final Button tryAgainButton = new Button("Try again", this::onTryAgainClick);

    public DeploymentView(DeploymentService deploymentService, Updater updater) {
        this.deploymentService = deploymentService;
        this.updater = updater;

        init();
        setLayout();
        gridInit();
    }

    private void init() {
        deploymentStatusSelect.setItems(DeploymentStatus.values());
        deploymentStatusSelect.addValueChangeListener(this::onDeploymentStatusChange);

        grid.removeAllColumns();
        grid.addColumn(Deployment::getDeploymentId).setHeader("Deployment ID");
        grid.addColumn(new ComponentRenderer<>(Deployment::getDeploymentStatusIcon)).setHeader("Status");

        grid.setItemDetailsRenderer(TemplateRenderer.<Deployment>of(messageTemplate())
                .withProperty("time", Deployment::time)
                .withProperty("message", Deployment::getMessage)
                .withEventHandler("handleClick", deployment -> grid.getDataProvider().refreshItem(deployment)));

        // Disable the default way of opening item details:
        grid.setDetailsVisibleOnClick(false);
        grid.addSelectionListener(this::onSelect);

        grid.addColumn(new NativeButtonRenderer<>("Message", item ->
                grid.setDetailsVisible(item, !grid.isDetailsVisible(item))))
                .setHeader("Message");

    }

    private void setLayout() {
        add(title, dataArea);
    }

    @Override
    protected void onAttach(AttachEvent attachEvent) {
        UI ui = attachEvent.getUI();
        broadcasterRegistration = Broadcaster.register(message -> ui.access(() -> {
            gridInit();

            if (StringUtils.isNotBlank(message)) {
                Notifier.doNotify(message).open();
            }
        }));
        //page init
        Broadcaster.broadcast("");
    }

    @Override
    protected void onDetach(DetachEvent detachEvent) {
        // Cleanup
        broadcasterRegistration.remove();
        broadcasterRegistration = null;
    }

    private void gridInit() {
        dataArea.removeAll();
        if (deploymentService.deploymentsQty() == 0) {
            dataArea.add(noItemsText);
        } else {
            dataArea.add(deploymentStatusSelect, grid);
            grid.setSelectionMode(Grid.SelectionMode.SINGLE);
            grid.setItems(deploymentService.getAllDeployments());
        }
    }

    private void onDeploymentStatusChange(AbstractField.ComponentValueChangeEvent<ComboBox<DeploymentStatus>, DeploymentStatus> changeEvent) {
        if (changeEvent.getValue() != null) {
            grid.setItems(deploymentService.getAllWithStatus(changeEvent.getValue()));
        } else {
            grid.setItems(deploymentService.getAllDeployments());
        }
    }

    private void onSelect(SelectionEvent<Grid<Deployment>, Deployment> gridDeploymentSelectionEvent) {
        Optional<Deployment> selection = gridDeploymentSelectionEvent.getFirstSelectedItem();
        selection.ifPresent(this::deployAgainIfFail);
    }

    private void deployAgainIfFail(Deployment deployment) {
        if (deployment.getDeploymentStatus() == DeploymentStatus.FAIL) {
            tryAgainButton.setId(deployment.getDeploymentId());
            add(tryAgainButton);
        } else {
            remove(tryAgainButton);
        }
    }

    private void onTryAgainClick(ClickEvent<Button> buttonClickEvent) {
        Optional<String> deploymentId = buttonClickEvent.getSource().getId();
        deploymentId.ifPresent(this::deployAgain);
    }

    private void deployAgain(String deploymentId) {
        Deployment deployment = deploymentService.getByDeploymentId(deploymentId);
        if (deployment != null) {
            updater.doUpdate(deployment.getImage());
            Notifier.doNotify("Update requested").open();
        } else {
            Notifier.doNotify("No such deployment: " + deploymentId).open();
        }
    }

    private String messageTemplate() {
        return "<div class='custom-details' style='border: 1px solid gray; padding: 10px; width: 100%; box-sizing: border-box;'>"
                + "<div>[[item.time]] </br> [[item.message]]</div>"
                + "</div>";
    }

}
