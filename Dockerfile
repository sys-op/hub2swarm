FROM java:8-jre
VOLUME /tmp
MAINTAINER Alexander Muravya (aka kyberorg) <kyberorg@yadev.eu>
ADD ./target/hub2swarm.jar /app/
RUN sh -c 'touch /app/hub2swarm.jar'

# This is not right way, use SPRING_PROFILES_ACTIVE instead
#ENV spring_profiles_active=dev

ENTRYPOINT exec java -Djava.security.egd=file:/dev/./urandom -jar /app/hub2swarm.jar
EXPOSE 8080
